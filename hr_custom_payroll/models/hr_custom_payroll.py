from openerp import models, fields, api, _ , tools
from datetime import date, datetime, timedelta
from openerp.osv import osv
import time, os, pprint, sys, math


class hr_payslip(models.Model):
    _name = 'hr.payslip'
    _inherit = 'hr.payslip'

    _defaults = {
        'monthly_attendance_id':    lambda self,cr,uid,ctx: self._get_default_monthly_id(cr, uid),
        'date_from':                lambda self,cr,uid,ctx: self._get_dates(cr, uid, 'start_date'),
        'date_to':                  lambda self,cr,uid,ctx: self._get_dates(cr, uid, 'end_date')

    }

    monthly_attendance_id = fields.Many2one(comodel_name="hr.monthly_attendance",string="Month Period", required=True, readonly=True, states={'draft': [('readonly', False)]})

    def _get_dates(self, cr, uid, date):
        monthly_id = self._get_default_monthly_id(cr, uid)
        if monthly_id:
            monthly_object = self.pool.get('hr.monthly_attendance').browse(cr, uid, monthly_id)

            dates = {'start_date': monthly_object.start_date, 'end_date': monthly_object.end_date}
            return dates[date]
        else:
            return None


    def _get_default_monthly_id(self, cr, uid):
        import_date = datetime.now().strftime("%B")+"-"+str(datetime.now().year)
        monthly_attendance_id = self.pool.get('hr.monthly_attendance').search(cr, uid, [('import_date', '=', import_date)])
        if len(monthly_attendance_id):
            return  monthly_attendance_id[0]
        else:
            return False

    @api.one
    @api.depends('name', 'employee_id')
    @api.onchange("monthly_attendance_id")
    def onchange_monthly_attendance_id(self):
        if self.monthly_attendance_id and self.employee_id:
            self.name       = 'Salary Slip of '+self.employee_id.name+' for '+self.monthly_attendance_id.import_date
            self.date_from  = self.monthly_attendance_id.start_date
            self.date_to    = self.monthly_attendance_id.end_date

        return True


    def create(self, cr, uid, vals, context=None):
        # Override date_from/date_to with monthly_attendance.start_date/end_date
        monthly_attendance = self.pool.get('hr.monthly_attendance').browse(cr, uid, vals.get('monthly_attendance_id'), context=context)
        vals.update({'date_from': monthly_attendance.start_date})
        vals.update({'date_to': monthly_attendance.end_date})

        return super(hr_payslip, self).create(cr, uid, vals, context=context)


    def onchange_employee_id(self, cr, uid, ids, date_from, date_to, employee_id=False, contract_id=False, monthly_attendance_id=False, context=None):
        empolyee_obj = self.pool.get('hr.employee')
        contract_obj = self.pool.get('hr.contract')
        worked_days_obj = self.pool.get('hr.payslip.worked_days')
        input_obj = self.pool.get('hr.payslip.input')
        # print(date_from, date_to)
        if context is None:
            context = {}
        #delete old worked days lines
        old_worked_days_ids = ids and worked_days_obj.search(cr, uid, [('payslip_id', '=', ids[0])], context=context) or False
        if old_worked_days_ids:
            worked_days_obj.unlink(cr, uid, old_worked_days_ids, context=context)

        #delete old input lines
        old_input_ids = ids and input_obj.search(cr, uid, [('payslip_id', '=', ids[0])], context=context) or False
        if old_input_ids:
            input_obj.unlink(cr, uid, old_input_ids, context=context)


        #defaults
        res = {'value':{
                      'line_ids':[],
                      'input_line_ids': [],
                      'worked_days_line_ids': [],
                      #'details_by_salary_head':[], TODO put me back
                      # 'name':'',
                      'contract_id': False,
                      'struct_id': False,
                      }
            }
        if (not employee_id) or (not date_from) or (not date_to):
            return res


        ttyme = datetime.fromtimestamp(time.mktime(time.strptime(date_from, "%Y-%m-%d")))
        employee_id = empolyee_obj.browse(cr, uid, employee_id, context=context)
        res['value'].update({
                    # 'name': _('Salary Slip of %s for %s') % (employee_id.name, tools.ustr(ttyme.strftime('%B-%Y'))),
                    'company_id': employee_id.company_id.id
        })

        if not context.get('contract', False):
            #fill with the first contract of the employee
            contract_ids = self.get_contract(cr, uid, employee_id, date_from, date_to, context=context)
        else:
            if contract_id:
                #set the list of contract for which the input have to be filled
                contract_ids = [contract_id]
            else:
                #if we don't give the contract, then the input to fill should be for all current contracts of the employee
                contract_ids = self.get_contract(cr, uid, employee_id, date_from, date_to, context=context)

        if not contract_ids:
            return res
        contract_record = contract_obj.browse(cr, uid, contract_ids[0], context=context)

        res['value'].update({
                    'contract_id': contract_record and contract_record.id or False
        })
        struct_record = contract_record and contract_record.struct_id or False
        if not struct_record:
            return res
        res['value'].update({
                    'struct_id': struct_record.id,
        })


        #computation of the salary input
        worked_days_line_ids = self.get_worked_day_lines(cr, uid, contract_ids, date_from, date_to, context=context)
        input_line_ids = self.get_inputs(cr, uid, contract_ids, date_from, date_to, context=context)

        # Add late hours amount to input lines
        if monthly_attendance_id:
            # Update name field
            monthly_object = self.pool.get('hr.monthly_attendance').browse(cr, uid, monthly_attendance_id)
            name       = 'Salary Slip of '+employee_id.name+' for '+monthly_object.import_date
            res['value'].update({
                        'name': name,
            })
            for input_line_id in input_line_ids:
                if input_line_id['code'] == 'LHRS': # The rule for late hours
                    # Multiply by -1 to convert to -ve
                    input_line_id['amount'] = self.pool.get('hr.monthly_attendance')._calculate_late_hours(cr, uid, employee_id.id, monthly_attendance_id, context=context) * -1

            res['value'].update({
                        'worked_days_line_ids': worked_days_line_ids,
                        'input_line_ids': input_line_ids,
            })
        return res

class hr_payslip_employees(models.Model):
    _name = 'hr.payslip.employees'
    _inherit = 'hr.payslip.employees'


    def compute_sheet(self, cr, uid, ids, context=None):
        emp_pool = self.pool.get('hr.employee')
        slip_pool = self.pool.get('hr.payslip')
        run_pool = self.pool.get('hr.payslip.run')

        slip_ids = []
        if context is None:
            context = {}
        data = self.read(cr, uid, ids, context=context)[0]
        run_data = {}
        if context and context.get('active_id', False):
            run_data = run_pool.read(cr, uid, [context['active_id']], ['date_start', 'date_end', 'credit_note'])[0]
        from_date =  run_data.get('date_start', False)
        to_date = run_data.get('date_end', False)
        credit_note = run_data.get('credit_note', False)

        if not data['employee_ids']:
            raise osv.except_osv(_("Warning!"), _("You must select employee(s) to generate payslip(s)."))

        monthly_attendance_obj = self.pool.get('hr.monthly_attendance').browse(cr, uid, slip_pool._get_default_monthly_id(cr, uid), context=context)

        for emp in emp_pool.browse(cr, uid, data['employee_ids'], context=context):
            slip_data = slip_pool.onchange_employee_id(cr, uid, [], from_date, to_date, emp.id, contract_id=False, context=context)
            name       = 'Salary Slip of '+emp.name+' for '+monthly_attendance_obj.import_date
            res = {
                'employee_id': emp.id,
                'name': name,
                'struct_id': slip_data['value'].get('struct_id', False),
                'contract_id': slip_data['value'].get('contract_id', False),
                'payslip_run_id': context.get('active_id', False),
                'input_line_ids': [(0, 0, x) for x in slip_data['value'].get('input_line_ids', False)],
                'worked_days_line_ids': [(0, 0, x) for x in slip_data['value'].get('worked_days_line_ids', False)],
                'date_from': from_date,
                'date_to': to_date,
                'credit_note': credit_note,
                'monthly_attendance_id': monthly_attendance_obj.id ,
            }
            slip_ids.append(slip_pool.create(cr, uid, res, context=context))
        slip_pool.compute_sheet(cr, uid, slip_ids, context=context)
        return {'type': 'ir.actions.act_window_close'}


class hr_payslip_run(models.Model):
    _name       = 'hr.payslip.run'
    _inherit    = 'hr.payslip.run'

    _defaults = {
        'state': 'draft',
        'monthly_attendance_id':    lambda self,cr,uid,ctx: self._get_default_monthly_id(cr, uid),
        'date_start':                lambda self,cr,uid,ctx: self._get_dates(cr, uid, 'start_date'),
        'date_end':                  lambda self,cr,uid,ctx: self._get_dates(cr, uid, 'end_date')
    }

    monthly_attendance_id = fields.Many2one(comodel_name="hr.monthly_attendance", string="Month Period", required=True, readonly=True, states={'draft': [('readonly', False)]})

    def _get_default_monthly_id(self, cr, uid):
        return self.pool.get('hr.payslip')._get_default_monthly_id(cr, uid)

    def _get_dates(self, cr, uid, date):
        return self.pool.get('hr.payslip')._get_dates(cr, uid, date)


    @api.one
    @api.onchange("monthly_attendance_id")
    def onchange_monthly_attendance_id(self):
        if self.monthly_attendance_id:
            self.date_start  = self.monthly_attendance_id.start_date
            self.date_end    = self.monthly_attendance_id.end_date

        return True