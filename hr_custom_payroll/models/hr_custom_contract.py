from openerp import models, fields, api, exceptions

class hr_contract(models.Model):
    _name = 'hr.contract'
    _inherit = 'hr.contract'

    car_allowence       = fields.Float(string='Car Allowance', default=0)
    car_allowence_currency = fields.Many2one(comodel_name='res.currency',string='Currency', )

    mobile_allowence    = fields.Float(string='Mobile Allowance', default=0)
    mobile_allowence_currency = fields.Many2one(comodel_name='res.currency',string='Currency', )

    working_hours       = fields.Many2one(comodel_name='resource.calendar',string='Working Schedule', required=True)
    company_id          = fields.Many2one(comodel_name='res.company',string='Company')
    employee_department = fields.Many2one(comodel_name='hr.department',string='Department')
    wage                = fields.Float(digits=(16,2), required=True, help="Basic Salary of the employee", string="Basic Salary") #, compute="compute_wage")

    field_rate          = fields.Integer(string="Field Rate / day")
    field_rate_currency = fields.Many2one(comodel_name='res.currency',string='Currency', )

    variable_bonus      = fields.Integer(string="Variable Bonus/ monh")
    variable_bonus_currency = fields.Many2one(comodel_name='res.currency',string='Currency', )


    #
    # @api.onchange('employee_id')
    # def _onchange_employee(self):
    #     raise ("looool")
    #     if self.employee_id:
    #         self.car_allowence_currency = self.employee_id.company_id.currency_id.id

    @api.one
    @api.onchange("car_allowence", "mobile_allowence")
    def onchange_allowence(self):
        self.wage = self.car_allowence + self.mobile_allowence + self.wage

    def onchange_employee_id(self, cr, uid, ids, employee_id, context=None):
        if not employee_id:
            return {'value': {'job_id': False}}
        emp_obj = self.pool.get('hr.employee').browse(cr, uid, employee_id, context=context)
        job_id = False
        emp_company_currency = emp_obj.company_id.currency_id.id
        if emp_obj.job_id:
            job_id = emp_obj.job_id.id
        if emp_obj.department_id:
            dep_id = emp_obj.department_id.id
        return {'value': {'job_id': job_id,
                          'employee_department': dep_id,
                          'car_allowence_currency':emp_company_currency,
                          'mobile_allowence_currency':emp_company_currency,
                          'field_rate_currency':emp_company_currency,
                          'variable_bonus_currency':emp_company_currency,
                          'company_id':emp_obj.company_id.id
                          }}

    @api.one
    @api.depends('car_allowence','mobile_allowence')
    def compute_wage(self):
        # Date Set
        self.wage = self.car_allowence + self.mobile_allowence

