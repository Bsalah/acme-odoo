from openerp import models, fields, api, _ , tools
from datetime import date, datetime, timedelta
from openerp.osv import osv
import time, os, pprint, sys, math


class resource_calendar_attendance(models.Model):
    _name = 'resource.calendar.attendance'
    _inherit = 'resource.calendar.attendance'

    name                = fields.Char(string="Name", required=True, default="Monday")
    custom_minute_from        = fields.Selection([('00','00'),
                                            ('15','15'),
                                            ('30','30'),
                                            ('45','45'),], string="Min (From)",default='00',required="True")
    custom_hour_from = fields.Selection([
            ('00','00'),
            ('01','01'),
            ('02','02'),
            ('03','03'),
            ('04','04'),
            ('05','05'),
            ('06','06'),
            ('07','07'),
            ('08','08'),
            ('09','09'),
            ('10','10'),
            ('11','11'),
            ('12','12'),
            ('13','13'),
            ('14','14'),
            ('15','15'),
            ('16','16'),
            ('17','17'),
            ('18','18'),
            ('19','19'),
            ('20','20'),
            ('21','21'),
            ('22','22'),
            ('23','23'),
        ], string="Hr (From)",default='09',required="True")




    custom_minute_to        = fields.Selection([('00','00'),
                                            ('15','15'),
                                            ('30','30'),
                                            ('45','45'),], string="Min (To)",default='00',required="True")
    custom_hour_to = fields.Selection([
            ('00','00'),
            ('01','01'),
            ('02','02'),
            ('03','03'),
            ('04','04'),
            ('05','05'),
            ('06','06'),
            ('07','07'),
            ('08','08'),
            ('09','09'),
            ('10','10'),
            ('11','11'),
            ('12','12'),
            ('13','13'),
            ('14','14'),
            ('15','15'),
            ('16','16'),
            ('17','17'),
            ('18','18'),
            ('19','19'),
            ('20','20'),
            ('21','21'),
            ('22','22'),
            ('23','23'),
        ], string="Hr (To)",default='17',required="True")


    @api.one
    @api.onchange("dayofweek")
    def onchange_dayofweek(self):
        days = {'0': 'Monday', '1': 'Tuesday', '2': 'Wednesday', '3': 'Thursday', '4': 'Friday', '5': 'Saturday', '6': 'Sunday'}
        if self.dayofweek:
            self.name = days[self.dayofweek]
        return True

    # def create(self, cr, uid, vals, context=None):
    #     # Save hour_from/hour_to from custom fields
    #
    #     vals.update({'hour_from':   str(vals.get('custom_hour_from')+":"+vals.get('custom_minute_from'))})
    #     vals.update({'hour_to':     str(vals.get('custom_hour_to')+":"+vals.get('custom_minute_to'))})
    #
    #     return super(resource_calendar_attendance, self).create(cr, uid, vals, context=context)


    # hour_from               = fields.Float('Work from', required=True, help="Start and End time of working.", select=True)
    # hour_to                 = fields.Float("Work to", required=True)

