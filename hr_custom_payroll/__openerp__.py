# -*- coding: utf-8 -*-
{
    'name': "Custom Payroll",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Your Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr', 'hr_payroll', 'hr_contract', 'hr_custom_attendance'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'data/salary_rules.xml',
        'templates.xml',
        'views/custom_resource_calendar_attendance_view.xml',
        'views/hr_custom_payslip_view.xml',
        'views/hr_custom_payslip_run_view.xml',
        'views/hr_custom_contract_view.xml',
        'hr_custom_payroll_report.xml',
        'report/hr_custom_payroll_report_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}