from openerp.osv import osv
from openerp.report import report_sxw
from lxml import html

class custom_payroll_report(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(custom_payroll_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            # 'time': time,
            # 'get_length': self._get_length,
            'text': self.text
            # 'get_counter_list': self._get_counter_list
        })

    # def _get_length(self, strings):
    #     return len(str(strings))

    def text(self, x):
        if x == False:
            x="None"
        doc = html.document_fromstring(x)
        text = doc.text_content()
        return text

class report_custom_payroll_report(osv.AbstractModel):
    _name = 'report.hr_custom_payroll.report_custom_payroll_temp'
    _inherit = 'report.abstract_report'
    _template = 'hr_custom_payroll.report_custom_payroll_temp'
    _wrapped_report_class = custom_payroll_report