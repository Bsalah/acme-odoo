from openerp import models, fields

class hr_family(models.Model):
    _name = 'hr.family'

    member_name     = fields.Char(string="Name", required=True)
    date_of_birth   = fields.Date(string='Date of Birth')
    relationship    = fields.Selection(string="Relationship", selection=[('child', 'Child'), ('spouse', 'Spouse'), ('father', 'Father'), ('mother', 'Mother')], required=True)
    employee_id     = fields.Many2one(comodel_name= 'hr.employee', string='Employee')

class hr_employee(models.Model):
    # Inherit from hr.employee
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    family_ids  = fields.One2many(comodel_name='hr.family', inverse_name='employee_id', string='Family Member')