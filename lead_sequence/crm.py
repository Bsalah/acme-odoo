from openerp.osv import fields, osv
import pprint , sys

class crm_lead(osv.osv):
    _inherit = "crm.lead"

    def _get_code(self, cr, uid,context, *args):
        obj_sequence = self.pool.get('ir.sequence')
        return obj_sequence.next_by_code(cr, uid, 'crm.lead.sequence', context=context)

    _columns = { 'code' : fields.char('Code', size=64), }



    def create(self, cr, uid, vals, context=None):
        context = dict(context or {})
        if vals.get('type') and not context.get('default_type'):
            context['default_type'] = vals.get('type')
        if vals.get('section_id') and not context.get('default_section_id'):
            context['default_section_id'] = vals.get('section_id')
        if vals.get('user_id'):
            vals['date_open'] = fields.datetime.now()


        create_context = dict(context, mail_create_nolog=True)
        if vals.get('type') == 'opportunity' :
           vals.update({'code': self._get_code(cr, uid, context)})

        return super(crm_lead, self).create(cr, uid, vals, context=create_context)

crm_lead()

class crm_lead2opportunity_partner(osv.osv):
    _inherit = "crm.lead2opportunity.partner"

    def action_apply(self, cr, uid, ids, context=None):



        """
        Convert lead to opportunity or merge lead and opportunity and open
        the freshly created opportunity view.
        """

        # generating next sequence
        obj_sequence = self.pool.get('ir.sequence')
        sequence = obj_sequence.next_by_code(cr, uid, 'crm.lead.sequence', context=context)


        if context is None:
            context = {}

        lead_obj = self.pool['crm.lead']

        w = self.browse(cr, uid, ids, context=context)[0]
        opp_ids = [o.id for o in w.opportunity_ids]
        vals = {
            'section_id': w.section_id.id
        }
        
        if w.partner_id:
            vals['partner_id'] = w.partner_id.id
            
        if w.name == 'merge':
            lead_id = lead_obj.merge_opportunity(cr, uid, opp_ids, context=context)
            lead_ids = [lead_id]
            lead = lead_obj.read(cr, uid, lead_id, ['type', 'user_id'], context=context)

            if lead['type'] == "lead":
                context = dict(context, active_ids=lead_ids)
                vals.update({'lead_ids': lead_ids, 'user_ids': [w.user_id.id]})
                self._convert_opportunity(cr, uid, ids, vals, context=context)
                
            elif not context.get('no_force_assignation') or not lead['user_id']:
                vals.update({'user_id': w.user_id.id, 'code': sequence})
                lead_obj.write(cr, uid, lead_id, vals, context=context)
                
        else:
            lead_ids = context.get('active_ids', [])
            vals.update({'lead_ids': lead_ids, 'user_ids': [w.user_id.id]})
            self._convert_opportunity(cr, uid, ids, vals, context=context)
            

        
        lead_obj.write(cr, uid, lead_ids, {'code': sequence}, context=context)
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(lead_obj)
        #sys.exit()
        return self.pool.get('crm.lead').redirect_opportunity_view(cr, uid, lead_ids[0], context=context)




crm_lead2opportunity_partner()