{
    'name': 'CRM Sequence',
    'version': '1.0',
    'category': 'CRM',
    'description': """ CRM Leads Sequence """,
    'author': 'ACME Group',
    'website': 'https://www.acme-group.net/',
    'depends': ['crm',],
    'data': ['crm_view.xml' , 'lead_view.xml'],
    'demo': [],
    'test':[],
    'init_xml': ["crm_view.xml"],
    'installable': True,
    'auto_install': True,
}

