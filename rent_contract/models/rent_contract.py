from openerp import models, fields, api, _
import re, sys
from datetime import datetime
from openerp.osv import osv
import datetime
import logging
import time
from dateutil.relativedelta import relativedelta

class account_analytic_account(models.Model):
    # inherit = name (make changes in the same DB , do not create new DB)
    _name = 'account.analytic.account'
    _inherit = 'account.analytic.account'

    recurring_invoices = fields.Boolean(string="Generate recurring invoices automatically", default=True)
    counter = fields.Integer(string="Counter", default=1)

    bank_name = fields.Char(string="Bank Name")
    bank_number = fields.Char(string="Bank Account#")

    unit_name = fields.Char(string="Unit")
    property_name = fields.Char(string="Property")

    price_1 = fields.Float(string='Price #1')
    date_1  = fields.Date(string='Date #1')

    price_2 = fields.Float(string='Price #2')
    date_2  = fields.Date(string='Date #2')

    price_3 = fields.Float(string='Price #3')
    date_3  = fields.Date(string='Date #3')

    price_4 = fields.Float(string='Price #4')
    date_4  = fields.Date(string='Date #4')

    price_5 = fields.Float(string='Price #5')
    date_5  = fields.Date(string='Date #5')

    def add_year(self, cr, uid, ids, context=None):
        account_analytic_account = self.browse(cr, uid, ids[0], context=None)
        if account_analytic_account.counter < 16:
            # print(account_analytic_account.counter + 1)
            self.write(cr, uid, ids, {
                'counter': account_analytic_account.counter + 1,
            }, context=context)
            return True

    def remove_year(self, cr, uid, ids, context=None):
        account_analytic_account = self.browse(cr, uid, ids[0], context=None)
        if account_analytic_account.counter > 1:
            account_analytic_account = self.browse(cr, uid, ids[0], context=None)
            self.write(cr, uid, ids, {
                'price_'+str(account_analytic_account.counter):None,
                'date_'+str(account_analytic_account.counter): None,
                'counter': account_analytic_account.counter - 1
            }, context=context)
            return True


    def recurring_create_invoice(self, cr, uid, ids, context=None):
        return self._recurring_create_invoice(cr, uid, ids, context=context)

    def _cron_recurring_create_invoice(self, cr, uid, context=None):
        return self._recurring_create_invoice(cr, uid, [], automatic=True, context=context)

    def _recurring_create_invoice(self, cr, uid, ids, automatic=False, context=None):
        context = context or {}
        invoice_ids = []
        current_date =  time.strftime('%Y-%m-%d')

        if ids:
            contract_ids = ids
            # print(contract_ids)
        else:
            contract_ids = self.search(cr, uid, [('recurring_next_date','<=', current_date), ('state','=', 'open'), ('recurring_invoices','=', True), ('type', '=', 'contract')])


        if contract_ids:
            cr.execute('SELECT company_id, array_agg(id) as ids FROM account_analytic_account WHERE id IN %s GROUP BY company_id', (tuple(contract_ids),))
            for company_id, ids in cr.fetchall():

                for contract in self.browse(cr, uid, ids, context=dict(context, company_id=company_id, force_company=company_id)):
                    try:
                        #while its between start and end date keep adding invoices
                        st_date = datetime.datetime.strptime(contract.date_start, "%Y-%m-%d")
                        end_date = datetime.datetime.strptime(contract.date, "%Y-%m-%d")
                        next_date = datetime.datetime.strptime(contract.recurring_next_date or current_date, "%Y-%m-%d")
                        interval = contract.recurring_interval
                        new_date = next_date

                        while(new_date < end_date):
                            # print(new_date)
                            invoice_values = self._prepare_invoice(cr, uid, contract, context=context)
                            if(invoice_values['invoice_line']):
                                if(contract.price_1 and contract.date_1 and new_date < datetime.datetime.strptime(contract.date_1, "%Y-%m-%d")):
                                    current_price = contract.price_1
                                elif(contract.price_2 and contract.date_2 and (datetime.datetime.strptime(contract.date_1, "%Y-%m-%d") <= new_date < datetime.datetime.strptime(contract.date_2, "%Y-%m-%d"))):
                                    current_price = contract.price_2
                                elif(contract.price_3 and contract.date_3 and (datetime.datetime.strptime(contract.date_2, "%Y-%m-%d") <= new_date < datetime.datetime.strptime(contract.date_3, "%Y-%m-%d"))):
                                    current_price = contract.price_3
                                elif(contract.price_4 and contract.date_4 and (datetime.datetime.strptime(contract.date_3, "%Y-%m-%d") <= new_date < datetime.datetime.strptime(contract.date_4, "%Y-%m-%d"))):
                                    current_price = contract.price_4
                                elif(contract.price_5 and contract.date_5 and (datetime.datetime.strptime(contract.date_4, "%Y-%m-%d") <= new_date < datetime.datetime.strptime(contract.date_5, "%Y-%m-%d"))):
                                    current_price = contract.price_5

                            else:
                                raise osv.except_osv(
                                _('WARNING'),
                                _('Please select a unit to invoice'))

                            # print(invoice_values)
                            invoice_values['invoice_line'][0][2]['price_unit'] = current_price
                            # invoice_values['state'] = 'open'

                            unit_pool = self.pool.get('product.template')
                            unit_object = unit_pool.browse(cr, uid, invoice_values['invoice_line'][0][2]['product_id'], context=None)

                            unit_name= unit_object.name
                            property_name = unit_object.property.name

                            sales_person_id = unit_object.owner_id.id

                            invoice_values['unit_name'] = unit_name
                            invoice_values['property_name'] = property_name

                            invoice_values['bank_name'] = contract.bank_name
                            invoice_values['bank_number'] = contract.bank_number

                            invoice_values['user_id'] = sales_person_id

                            id = self.pool['account.invoice'].create(cr, uid, invoice_values, context=context)
                            invoice_ids.append(id)
                            # print(invoice_values)
                            if(id):
                                ################################### validate the created invoice
                                proxy = self.pool['account.invoice']
                                for record in proxy.browse(cr, uid, [id], context=context):
                                    if record.state not in ('draft', 'proforma', 'proforma2'):
                                        raise osv.except_osv(_('Warning!'), _("Selected invoice(s) cannot be confirmed as they are not in 'Draft' or 'Pro-Forma' state."))
                                    record.signal_workflow('invoice_open')
                                ###################################
                                product_id = invoice_values['invoice_line'][0][2]['product_id']
                                product_name = invoice_values['invoice_line'][0][2]['name']

                                unit_pool.write(cr, uid, [product_id], {
                                    'next_vacancy': end_date,
                                    'current_tenant': contract.partner_id.name
                                }, context=context)

                                tenant_pool = self.pool.get('res.partner')

                                tenant_pool.write(cr, uid, [contract.partner_id.id], {
                                    'move_in': st_date,
                                    'move_out': end_date,
                                    'unit': product_name,
                                    'bank_name': contract.bank_name,
                                    'bank_number': contract.bank_number
                                }, context=context)
                            # print(unit_name)
                            if contract.recurring_rule_type == 'daily':
                                new_date = next_date+relativedelta(days=+interval)
                            elif contract.recurring_rule_type == 'weekly':
                                new_date = next_date+relativedelta(weeks=+interval)
                            elif contract.recurring_rule_type == 'monthly':
                                new_date = next_date+relativedelta(months=+interval)
                            else:
                                new_date = next_date+relativedelta(years=+interval)
                            next_date = new_date
                            self.write(cr, uid, [contract.id], {'recurring_next_date': new_date.strftime('%Y-%m-%d')}, context=context)
                        if automatic:
                            cr.commit()
                    except Exception:
                        if automatic:
                            cr.rollback()
                            _logger.exception('Fail to create recurring invoice for contract %s', contract.code)
                        else:
                            raise
        return invoice_ids

    def set_close(self, cr, uid, ids, context=None):
        # return self.write(cr, uid, ids, {'state': 'close'}, context=context)
        # print("looooooooooooooooooooooooooooooooooooooooooooooooool")

        if ids:
            contract_ids = ids
            # print(contract_ids)
        # else:
        #     contract_ids = self.search(cr, uid, [('state','=', 'open'), ('recurring_invoices','=', True), ('type', '=', 'contract')])


        if contract_ids:
            cr.execute('SELECT company_id, array_agg(id) as ids FROM account_analytic_account WHERE id IN %s GROUP BY company_id', (tuple(contract_ids),))
            for company_id, ids in cr.fetchall():
                for contract in self.browse(cr, uid, ids, context=dict(context, company_id=company_id, force_company=company_id)):
                    # try:
                    # print(invoice_values)
                    invoice_values = self._prepare_invoice(cr, uid, contract, context=context)
                    product_id = invoice_values['invoice_line'][0][2]['product_id']

                    unit_pool = self.pool.get('product.template')

                    unit_pool.write(cr, uid, [product_id], {
                        'next_vacancy': None,
                        'current_tenant': None
                    }, context=context)

                    tenant_pool = self.pool.get('res.partner')

                    tenant_pool.write(cr, uid, [contract.partner_id.id], {
                        'move_in': None,
                        'move_out': None,
                        'unit': None
                    }, context=context)

                    return self.write(cr, uid, ids, {'state': 'close'}, context=context)


class account_invoice(models.Model):
    # inherit = name (make changes in the same DB , do not create new DB)
    _name = 'account.invoice'
    _inherit = 'account.invoice'


    unit_name = fields.Char(string="Unit")
    property_name = fields.Char(string="Property")
    bank_name = fields.Char(string="Bank Name")
    bank_number = fields.Char(string="Bank Account#")