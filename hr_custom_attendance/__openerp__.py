# -*- coding: utf-8 -*-
{
    'name': "HR Custom Attendance",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """

    """,

    'author': "Your Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr','hr_holidays'],

    # always loaded
    'data': [
        'security/hr_custom_attendance_security.xml',
        'security/ir.model.access.csv',
        'views/hr_custom_absence_view.xml',
        'views/hr_custom_attendance_view.xml',
        'views/hr_monthly_attendance_view.xml',
    ],

    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}