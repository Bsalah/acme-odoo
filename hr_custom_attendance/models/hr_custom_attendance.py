from openerp import models, fields, api, _ , tools
from datetime import date, datetime, timedelta
from openerp.osv import osv
import os, pprint, sys, math
from collections import defaultdict
import traceback

class hr_custom_attendance(models.Model):
    _name       = 'hr.custom_attendance'
    _rec_name   = 'employee_id'
    _order = 'attendance_date asc'
    _inherit = 'ir.needaction_mixin'

    employee_id             = fields.Many2one(comodel_name='hr.employee', string='Employee')
    monthly_attendance_id   = fields.Many2one(comodel_name='hr.monthly_attendance', string='Employee', ondelete="cascade",required=True)
    signin_datetime         = fields.Datetime(string='SignIn Date')
    signout_datetime        = fields.Datetime(string='SignOut Date')
    attendance_date         = fields.Date(string="Attendance Date")
    attendance_date_string  = fields.Char(string="Attendance Date String")
    working_hours           = fields.Float(string="Working Hours")
    late_hours              = fields.Float(string="Late Hours")
    state                   = fields.Selection(selection=[('ignored', 'Ignored'),('penalty_applied','Penalty Applied')], readonly='True', required='True', default='ignored')
    absent                  = fields.Boolean(string="Absent", default=False)
    approval_date           = fields.Date(string="Approval Date")
    system_generated        = fields.Boolean(string="System Generated", default=False)

    def action_button_apply_penalty(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {
            'state':            'penalty_applied',
        }, context=context)

    # def _needaction_domain_get(self, cr, uid, context=None):
    #         domain = [('state', '=', 'penalty_applied')]
    #         return domain


#############################################################################################
class hr_holidays(models.Model):
    _name = "hr.holidays"
    _inherit = "hr.holidays"
    absence_id = fields.Many2one(comodel_name='hr.custom_absence', ondelete="cascade")

#############################################################################################


class hr_monthly_attendance(models.Model):
    _name       = 'hr.monthly_attendance'
    _rec_name   = 'import_date'

    import_month        = fields.Selection(selection=[('January','January'),('February','February'),('March','March'),('April','April'),('May','May'),('June','June'),('July','July'),('August','August'),('September','September'),('October','October'),('November','November'),('December','December')],defualt="March",string='Import Month', required=True)
    import_year         = fields.Selection([(num, str(num)) for num in range(2011, (datetime.now().year)+1 )], string='Import Year', required=True)
    import_date         = fields.Char(string='Import Date')
    start_date          = fields.Date(string="Start Date", required=True)
    end_date            = fields.Date(string="End Date", required=True)


    _defaults = {
        'import_month': lambda self,cr,uid,ctx: datetime.now().strftime("%B"),
        'import_year': lambda self,cr,uid,ctx: datetime.now().year
    }

    def create(self, cr, uid, data, context=None):
        #concatinate month & year
        data.update({'import_date': str(data.get("import_month"))+"-"+str(data.get("import_year"))})
        #check to create monthly
        monthly_id          = self.get_monthly_id(cr, uid, str(data.get("import_month")), str(data.get("import_year")), context=None)
        attendance_file     = self.check_monthly_file(str(data.get("import_month")), str(data.get("import_year")))
        start_end_date      = self.check_start_end_date(datetime.strptime(data.get("start_date"), "%Y-%m-%d"), datetime.strptime(data.get("end_date"), "%Y-%m-%d"))

        try:
            if (not monthly_id) and (attendance_file) and (start_end_date): #and (not companies): # no ID AND attendance_file exist
                monthly_id = super(hr_monthly_attendance, self).create(cr, uid, data, context=context)
                if(monthly_id):
                    try:
                        # employees = self.pool.get('hr.employee').search(cr, uid, [], context=context)
                        # for employee_id in employees:
                        #     employee = self.pool.get('hr.employee').browse(cr, uid, employee_id, context=context)
                        #
                        #     self.pool.get('hr.contract').create(cr, uid,{
                        #                 'employee_id':              employee.id,
                        #                 'working_hours':            1,
                        #                 'job_id':                   employee.job_id.id,
                        #                 'name':                     employee.name+" 2015",
                        #                 'type_id':                  1,
                        #                 'date_start':               '2015-01-01',
                        #                 'date_end':                 '2015-12-31',
                        #                 'wage':                     10000,
                        #                 'schedule_pay':             'monthly',
                        #                 'struct_id':                1,
                        #                 'car_allowence':            0,
                        #                 'mobile_allowence':         0
                        #                 }, context=context)

                        # emp_pool.write(cr, uid, [employee_id], {
                        #             'employee_id':              employee.employee_id[1:]
                        #             }, context=context)

                        self._import_file(cr, uid, attendance_file, monthly_id, datetime.strptime(data.get("start_date"), "%Y-%m-%d"), datetime.strptime(data.get("end_date"), "%Y-%m-%d"), context=None)
                        return monthly_id
                    except Exception as e:
                        raise Exception(e.message)
            else:
                if not attendance_file:
                    raise Exception("The file for this date does not exist")
                if monthly_id:
                    raise Exception("Monthly attendance with the same date already exist")
                if not start_end_date:
                    raise Exception("StartDate can not be greater than or equal to EndDate")
        except Exception as e:
            raise osv.except_osv(_("Warning"), _(e.message))


    def _check_companies_config(self, cr):
        query = """ SELECT name FROM res_company WHERE working_hours IS NULL OR sign_in_hours IS NULL OR working_hours = 0 OR sign_in_hours = '00' """
        cr.execute(query)
        companies = cr.fetchall()
        company_list = ""
        if len(companies):
            for company in companies:
                company_list = company_list + company[0] + "\n"
            return company_list
        else:
            return False

    def get_monthly_id(self,cr, uid, month, year, context=None):
        monthly_attendance = self.search(cr, uid, [('import_month','=',month),('import_year','=',year)], context=context)
        if len(monthly_attendance):
            return monthly_attendance[0] # return ID
        else:
            return False



    def check_monthly_file(self,month, year):
        file = os.getcwd()+"/Attendance/"+str(month)+"-"+str(year)+".txt"
        if os.path.isfile(file):
            return file
        else:
            return False


    def check_start_end_date(self,start_date, end_date):
        if start_date >= end_date:
            return False
        else:
            return True




    def _import_file(self, cr, uid, file, monthly_id, start_date, end_date, context=None):
        file_array = open(file, 'r')
        try:
            names = []
            dates = []
            # pp = pprint.PrettyPrinter(indent=4)
            # pp.pprint(file_array)
            # sys.exit()
            for row in file_array:
                # pp = pprint.PrettyPrinter(indent=4)
                # pp.pprint()
                # sys.exit()

                emp_id = ""
                # Checking on employee_id length (should be 4 digits)
                if len(str(row.split()[0])) == 1:
                    emp_id = "000"+str(row.split()[0])
                elif len(str(row.split()[0])) == 2:
                    emp_id = "00"+str(row.split()[0])
                elif len(str(row.split()[0])) == 3:
                    emp_id = "0"+str(row.split()[0])
                else:
                    emp_id = str(row.split()[0])

                names.append(emp_id)
                dates.append(row.split()[1])

            file_array.close()
            attendance_array = {names_k:{dates_k:[] for dates_k in dates} for names_k in names}

            file_array = open(file, 'r')
            for row in file_array:
                dt      = row.split()[1]+" "+row.split()[2]
                my_date = datetime.strptime(dt+":00", "%d/%m/%Y %H:%M:%S")

                if row.split()[3] == '010': # Sign-in
                    if len(attendance_array[emp_id][row.split()[1]]) == 0: # Figuring first sign in to use in calculating late hours
                        first_signin = True
                    else:
                        first_signin = False

                    attendance_array[emp_id][row.split()[1]].append({'010': my_date, '011': None, 'system_generated': False, 'first_signin': first_signin})

                if row.split()[3] == '011': # Sign-out
                    if len(attendance_array[emp_id][row.split()[1]]) > 0: # if exist array of in_outs
                        if attendance_array[emp_id][row.split()[1]][-1]['011'] is None: # if last array[out] = None
                            attendance_array[emp_id][row.split()[1]][-1]['011'] = my_date
                        else:  # if last array of out is filled
                            # first_signin will be false; In this case the employee was staying up late working
                            attendance_array[emp_id][row.split()[1]].append({'010': datetime.strptime(row.split()[1]+" 00:00:01", "%d/%m/%Y %H:%M:%S"), '011': my_date, 'system_generated': True, 'first_signin': False})
                    else:  # if array is empty
                        # first_signin will be false; In this case the employee was staying up late working
                        attendance_array[emp_id][row.split()[1]].append({'010': datetime.strptime(row.split()[1]+" 00:00:01", "%d/%m/%Y %H:%M:%S"), '011': my_date, 'system_generated': True, 'first_signin': False})

            # pp = pprint.PrettyPrinter(indent=4)
            # pp.pprint(attendance_array)
            # sys.exit()
        except Exception as e:
            raise Exception(e.message)
        finally:
            file_array.close()

        # create attendance records
        try:
            self._create_attendance_records(cr, uid, attendance_array, monthly_id, start_date, end_date)
        except Exception as e:
            raise Exception(e.message)

    def _calculate_late_hours(self, cr, uid, employee_id, monthly_id, context=None):
        query = """ SELECT sum(ca.late_hours) from hr_custom_attendance as ca WHERE ca.employee_id = """+str(employee_id)+""" and ca.monthly_attendance_id = """+str(monthly_id)+""" and ca.state = 'penalty_applied' and ca.absent=FALSE """
        cr.execute(query)
        result = cr.dictfetchone()

        if result['sum'] != None:
            if result['sum'] <= 4: # 4 hours allowence
                # return 0
                return result['sum']
            else:
                return result['sum'] - 4
        return 0

    def _create_attendance_records(self,cr, uid, attendance_array, monthly_id, start_date, end_date, context=None):
        # pp = pprint.PrettyPrinter(indent=4)
        # pp.pprint(attendance_array)
        # sys.exit()
        try:
            no_contract = []
            no_employee = []
            # Insert records into custom_attendance
            for emp_id, dates_array in attendance_array.iteritems():
                employee_id = self.pool.get('hr.employee').search(cr, uid, [('employee_id','=',str(emp_id))], context=context)

                if len(employee_id):
                    employee    = self.pool.get('hr.employee').browse(cr, uid, employee_id[0], context=context)

                    # Get contract by employee_id
                    contract_id    = self.pool.get('hr.payslip').get_contract(cr, uid, employee, start_date, end_date, context=context)
                    if len(contract_id) == 0:
                        no_contract.append(employee.name)
                        continue
                    else:
                        contract_id = contract_id[0]

                else:
                    # raise Exception("Employee with ID='"+emp_id+"' was not found. Please check your IDs and try again. Delete this monthly attendance if created.")
                    no_employee.append(emp_id)
                    continue

                # Loop on range of dates in monthly
                my_date = start_date
                delta   = timedelta(days=1)

                while my_date <= end_date:
                    # get working hours for this specific 'day' for this employee
                    working_hours = self._get_working_hours_by_day(cr, contract_id, employee.id, my_date)

                    if (not working_hours): # Weekend
                        my_date += delta
                        continue

                    # Initiate late_hours = 0
                    late_hours = 0
                    attendance_date = my_date.strftime("%d/%m/%Y")
                    # Concatenating attendance_date with working_days HH:MM
                    company_signin_time     = datetime.strptime(attendance_date+" "+working_hours['custom_hour_from']+":"+working_hours['custom_minute_from'], "%d/%m/%Y %H:%M")
                    company_signout_time    = datetime.strptime(attendance_date+" "+working_hours['custom_hour_to']+":"+working_hours['custom_minute_to'], "%d/%m/%Y %H:%M")
                    company_working_hours   = self.datetime_difference(company_signout_time, company_signin_time)

                    if (my_date.strftime("%d/%m/%Y") in dates_array) and len(dates_array[my_date.strftime("%d/%m/%Y")]): # if date in file exist
                        for in_out in dates_array[my_date.strftime("%d/%m/%Y")]: # Loop on employee in_out for this date
                            # pp = pprint.PrettyPrinter(indent=4)
                            # pp.pprint(in_out_array['011'])
                            # sys.exit()

                            # check if any out = None and replace with (date + 23:59:59)
                            if in_out['011'] is None:
                                in_out['011']               = datetime.strptime(attendance_date+" 23:59:59", "%d/%m/%Y %H:%M:%S")
                                in_out['system_generated']  = True

                            # Attended hours = difference between signout - signin
                            attended_hours          = self.datetime_difference(in_out['011'], in_out['010'])

                            # Calculating late_hours
                            if in_out['first_signin']:
                                late_hours          = self.datetime_difference(in_out['010'], company_signin_time)
                                # If late hours is -ve --> then no latency
                                if late_hours < 0:
                                    late_hours = 0
                            else: # Reset to Zero
                                late_hours = 0


                            row_id = self._save_daily_attendance(cr, uid,
                                employee_id[0],                                                     #employee_id,
                                monthly_id,                                                         #monthly_attendance_id,
                                in_out['010'],                                                      #signin_datetime,
                                in_out['011'],                                                      #signout_datetime,
                                attended_hours,                                                     #attended_hours
                                late_hours,                                                         #late_hours
                                False,                                                              #Absent
                                in_out['system_generated'],                                         #system_generated
                                datetime.strptime(attendance_date, "%d/%m/%Y"),                     #attendance_date
                                datetime.strptime(attendance_date, "%d/%m/%Y").strftime("%Y-%m-%d"),#attendance_date_string
                                'ignored',                                                          #state
                                context                                                             #context
                            )
                    else: # create record for this absence

                        # Check if employee has leave requests on this day
                        leave_request = self._check_leave_request_exist(cr, uid, employee.id, datetime.strptime(attendance_date, "%d/%m/%Y").strftime("%Y-%m-%d"))
                        # Check if there is a public holiday on this day
                        try:
                            public_holiday = self._check_public_holiday_exist(cr, uid, datetime.strptime(attendance_date, "%d/%m/%Y").strftime("%Y-%m-%d"))
                        except Exception as e:
                            raise Exception(_("Public holidays must be installed in order to continue"))


                        if (not leave_request) and (not public_holiday):

                            row_id = self._save_daily_absence(cr, uid,
                                employee_id[0],                                                     #employee_id,
                                monthly_id,                                                         #monthly_attendance_id,
                                True,                                                               #Absent
                                True,                                                               #system_generated
                                datetime.strptime(attendance_date, "%d/%m/%Y"),                     #attendance_date
                                datetime.strptime(attendance_date, "%d/%m/%Y").strftime("%d/%m/%Y"),#attendance_date_string
                                '',                                                                 #state
                                context                                                             #context
                            )

                    my_date += delta

            if len(no_contract):
                names = ""
                for emp_name in no_contract:
                    names = names + emp_name + "\n"
                raise Exception("The following employees have no contracts: \n"+names)

            if len(no_employee):
                names = ""
                for emp_id in no_employee:
                    names = names + emp_id + "\n"
                raise Exception("The following IDs do not match any employee: \n"+names)


        except Exception as e:
            raise Exception(e.message)


    def _get_working_hours_by_day(self, cr, contract_id, employee_id, my_date):
        query = """ SELECT

                        rca.custom_hour_from,
                        rca.custom_minute_from,
                        rca.custom_hour_to,
                        rca.custom_minute_to

                        FROM resource_calendar_attendance as rca, resource_calendar as rc, hr_contract as hrc

                        WHERE

                        rca.calendar_id = rc.id and
                        hrc.working_hours = rc.id and
                        hrc.id = """+str(contract_id)+""" and
                        hrc.employee_id = """+str(employee_id)+""" and
                        rca.name = '"""+my_date.strftime("%A")+"""'
                    """

        cr.execute(query)
        return cr.dictfetchone()

    def _check_leave_request_exist(self, cr, uid, employee_id, check_date, context=None):
        query = """ SELECT id FROM hr_holidays WHERE date_from::timestamp::date <= '"""+check_date+"""' and date_to::timestamp::date >= '"""+check_date+"""' and employee_id = """+str(employee_id)+""" and holiday_type = 'employee' and type = 'remove' """
        cr.execute(query)
        result = cr.dictfetchone()
        if result:
            return result['id']
        else:
            return False

    def _check_public_holiday_exist(self, cr, uid, check_date, context=None):
        query = """ SELECT id FROM hr_public_holidays WHERE date_from::timestamp::date <= '"""+check_date+"""' and date_to::timestamp::date >= '"""+check_date+"""'"""
        cr.execute(query)
        result = cr.dictfetchone()
        if result:
            return result['id']
        else:
            return False

    def _save_daily_attendance(self, cr, uid, employee_id, monthly_id, signin_datetime, signout_datetime, attended_hours, late_hours, absent, system_generated, attendance_date, attendance_date_string, state, context):
        row_id = self.pool.get('hr.custom_attendance').create(cr, uid,{
                                'employee_id':              employee_id,
                                'monthly_attendance_id':    monthly_id,
                                'signin_datetime':          signin_datetime,
                                'signout_datetime':         signout_datetime,
                                'working_hours':            attended_hours,
                                'absent':                   absent,
                                'late_hours':               late_hours,
                                'system_generated':         system_generated,
                                'attendance_date':          attendance_date,
                                'attendance_date_string':   attendance_date_string,
                                'state':                    state,
                                }, context=context)
        return row_id

    def _save_daily_absence(self, cr, uid, employee_id, monthly_id, absent, system_generated, attendance_date, attendance_date_string, state, context):
        row_id = self.pool.get('hr.custom_absence').create(cr, uid,{
                                'employee_id':              employee_id,
                                'monthly_attendance_id':    monthly_id,
                                'absent':                   absent,
                                'system_generated':         system_generated,
                                'attendance_date':          attendance_date,
                                'attendance_date_string':   attendance_date_string,
                                'state':                    state,
                                }, context=context)
        return row_id

    # returns hrs (2 decimal places)
    def datetime_difference(self, last, first):
        # Calculate working Hours
        diff = last - first
        return round(diff.total_seconds()/60/60, 2)

    # def calculate_employee_monthly_attendance(self, cr, uid):

class hr_custom_absence(models.Model):
    _name       = 'hr.custom_absence'
    _rec_name   = 'employee_id'
    _order = 'attendance_date asc'
    _inherit = 'ir.needaction_mixin'

    employee_id             = fields.Many2one(comodel_name='hr.employee', string='Employee')
    monthly_attendance_id   = fields.Many2one(comodel_name='hr.monthly_attendance', string='Employee', ondelete="cascade",required=True)
    attendance_date         = fields.Date(string="Attendance Date")
    attendance_date_string  = fields.Char(string="Attendance Date String")
    state                   = fields.Selection(selection=[('business_day', 'Business Day'),('annual_leave','Annual Leave'),('unpaid_leave','Unpaid Leave')], required='True', default='business_day')
    absent                  = fields.Boolean(string="Absent", default=False)
    approval_date           = fields.Date(string="Approval Date")
    system_generated        = fields.Boolean(string="System Generated", default=False)

    def _needaction_domain_get(self, cr, uid, context=None):
            domain = [('state', '=', '')]
            return domain

    def write(self, cr , uid, ids, data, context=None):
        try:
            ### delete all hr holidays records related with this absence record
            if( not self.delete_annual_leaves(cr,uid,ids[0],context)):
                raise Exception("Error create new leave request")
            if data.get('state') == "annual_leave":
                absence_record = self.browse(cr, uid, ids[0], context=context)

                # Get Holiday Category 'Annual Leaves'
                holiday_status = self.pool.get('hr.holidays.status').search(cr, uid, [('name','=','Annual Leaves')], context=context)
                if len(holiday_status) == 0:
                    raise Exception("Please create 'Annual Leaves' category")

                # Checking if employee has manager
                try:
                    manager_id = absence_record.employee_id.parent_id.id
                except Exception as e:
                    manager_id = 1

                annual_leaves = self.get_annual_leaves(cr, absence_record.employee_id.id)
                if annual_leaves >= 1:
                    holiday_id = self.pool.get('hr.holidays').create(cr, uid,{
                                    'employee_id':              absence_record.employee_id.id,
                                    'holiday_status_id':        holiday_status[0],
                                    'date_from':                absence_record.attendance_date+" 05:00:00",
                                    'date_to':                  absence_record.attendance_date+" 17:00:00",
                                    'holiday_type':             'employee',
                                    'number_of_days_temp':      1,
                                    'state':                    'confirm',
                                    'type':                     'remove',
                                    'manager_id':               manager_id,
                                    'department_id':            absence_record.employee_id.department_id.id,
                                    'name':                     'System Generated',
                                    'number_of_days':           -1,
                                    'absence_id':               ids[0],
                                    'user_id':                  uid
                                    }, context=context)

                    # Approving leave request
                    function_return = self.pool.get('hr.holidays').holidays_validate(cr, uid, [holiday_id], context=context)
                    if not function_return:
                        raise Exception("There was a problem trying to save this record. Please refresh and try again")


            return super(hr_custom_absence, self).write(cr, uid, ids, data, context=context)
        except Exception as e:
            raise osv.except_osv(_("Warning"), _(e.message))

    def get_annual_leaves(self, cr, employee_id):
        query = """ select sum(number_of_days) from hr_holidays where employee_id = """+str(employee_id)+""" and state = 'validate' """

        cr.execute(query)
        result= cr.dictfetchone()
        if result['sum'] != None:
            return result['sum']
        return 0

    def get_number_of_unpaid_leaves(self,cr, employee_id, monthly_id):
        query = """ select count(id) as count from hr_custom_absence where employee_id = """+str(employee_id)+""" and state = 'unpaid_leave' and monthly_attendance_id="""+str(monthly_id)+""" """

        cr.execute(query)
        result= cr.dictfetchone()
        return result['count']

    def delete_annual_leaves(self,cr,uid,absence_id,context):
        try:
            query = """ delete from hr_holidays where absence_id = """+str(absence_id)+""""""
            cr.execute(query)
            return True
        except:
            return False