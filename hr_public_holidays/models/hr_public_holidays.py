import datetime
from openerp import models, fields, api, _ , tools
from datetime import date, datetime, timedelta
from openerp.osv import osv
import time, os, pprint, sys, math


class hr_public_holidays(models.Model):
    _name = "hr.public_holidays"
    # _rec_name = "description"

    description = fields.Char(string="Description" ,required="True")
    date_from = fields.Datetime(string="Start Date",required="True")
    date_to = fields.Datetime(string="End Date",required="True")
    number_of_days = fields.Integer(string="Number of days")
    # company_id = fields.Many2one(comodel_name="res.company",string="Company",required="True")

    @api.one
    @api.onchange("date_from","date_to")
    def check_diff(self):
        hr_holidays = self.pool.get('hr.holidays')
        self.number_of_days = hr_holidays._get_number_of_days(self.date_from,self.date_to)+1

        # return result