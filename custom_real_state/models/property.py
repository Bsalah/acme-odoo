from openerp import models, fields, api

class property(models.Model):
    _name = "property"

    name = fields.Char(string="Name")
    address = fields.Text(string="Address")
    floor_numbers = fields.Integer(string="Number of Floors")
    units_number = fields.Integer(string="Number of Units")
    rented_units_number = fields.Integer(string="Number of Rented Units")


