from openerp import models, fields, api

class tenant(models.Model):
    _name = "res.partner"
    _inherit = "res.partner"

    move_in = fields.Date(string="Move In Date", readonly=True)
    move_out = fields.Date(string="Move Out Date", readonly=True)
    unit = fields.Char(string="Unit", readonly=True)
    property = fields.Char(string="Property", readonly=True)
    bank_name = fields.Char(string="Bank Name")
    bank_number = fields.Char(string="Bank Account#")