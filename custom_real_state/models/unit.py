from openerp import models, fields, api

class unit(models.Model):
    _name = "product.template"
    _inherit = "product.template"

    # name = fields.Char(string="Code")
    rent = fields.Char(string="Rent")
    property = fields.Many2one(comodel_name="property",string="Property",required=True)
    floor = fields.Char(string="Floor",required=True)
    unit_number = fields.Char(string="Unit number",required=True)
    size = fields.Char(string="Size")
    # owner = fields.Many2one(comodel_name="res.users",string="Owner",required=True)
    owner_id = fields.Many2one(comodel_name="res.users",string="Owner",required=True)
    beds_number = fields.Integer(string="Number of Beds")
    bathrooms_number = fields.Integer(string="Number of Bathrooms")

    current_tenant = fields.Char(string="Current Tenant")
    next_vacancy = fields.Date(string="Next Vacancy")


    @api.one
    @api.onchange("property","floor","unit_number")
    def onchange_unit_info(self):
        if self.property.name and self.floor and self.unit_number:
            self.name = self.property.name +"-"+ self.floor +"-"+ self.unit_number
        return True

    # def create(self, cr , uid, vals, context=None):
    #     if self.property.name and self.floor and self.unit_number:
    #         self.name = self.property.name + self.floor + self.unit_number
    #     return True
