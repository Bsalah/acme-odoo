# -*- coding: utf-8 -*-
{
    'name': "Real Estate Acme",

    'summary': """

        Custom Real Estate

        """,

    'description': """
        Custom real estate
    """,

    'author': "ACME BP",
    'website': "http://www.acme-group.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','product','sale','project','account','account_analytic_analysis'],

    # always loaded
    'data': [
        # 'views/contract_view.xml',
        # 'views/invoice_view.xml',
        'views/tenant_view.xml',
        'views/unit_view.xml',
        'views/property_view.xml',
        'views/menuitems.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}