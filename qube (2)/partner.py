from openerp import models, fields, api, _
import re, sys
from openerp.osv import osv

class res_partner(osv.osv):

    _inherit = "res.partner"

    @api.one
    @api.constrains('phone', 'mobile','fax' ,'email')
    def _check_name_duplication(self):
        empty = 0

        if(self.phone):
            empty=1
            partner_id_phone = self.search([('phone', '=', str(self.phone)), ('id', '!=', self.id)])
            if len(partner_id_phone):
                raise Warning(_('Phone already exist'))
        if(self.mobile):
            empty=1
            partner_id_mobile = self.search([('mobile', '=', str(self.mobile)), ('id', '!=', self.id)])
            if len(partner_id_mobile):
                raise Warning(_('Mobile already exist'))
        if(self.fax):
            empty=1
            partner_id_fax = self.search([('fax', '=', str(self.fax)), ('id', '!=', self.id)])
            if len(partner_id_fax):
                raise Warning(_('Fax already exist'))
        if(self.email):
            empty=1
            partner_id_email = self.search([('email', '=', str(self.email)), ('id', '!=', self.id)])
            if len(partner_id_email):
                raise Warning(_('Email already exist'))

        if (empty == 0):
            raise Warning(_('You have to enter one of (Phone, Mobile, Mobile2, Email)'))


class crm_lead(osv.osv):
    _name = "crm.lead"
    _inherit = "crm.lead"

    @api.one
    @api.constrains('phone', 'mobile','fax' ,'email_from')
    def _check_name_duplication(self):
        empty = 0

        if(self.phone):
            empty=1
            partner_id_phone = self.search([('phone', '=', str(self.phone)), ('id', '!=', self.id)])
            if len(partner_id_phone):
                raise Warning(_('Phone already exist'))
        if(self.mobile):
            empty=1
            partner_id_mobile = self.search([('mobile', '=', str(self.mobile)), ('id', '!=', self.id)])
            if len(partner_id_mobile):
                raise Warning(_('Mobile already exist'))
            if (len(self.mobile) != 11):
                raise Warning(_('Mobile should be 11 digit'))
        if(self.fax):
            empty=1
            partner_id_fax = self.search([('fax', '=', str(self.fax)), ('id', '!=', self.id)])
            if len(partner_id_fax):
                raise Warning(_('Fax already exist'))
        if(self.email_from):
            empty=1
            partner_id_email = self.search([('email_from', '=', str(self.email_from)), ('id', '!=', self.id)])
            if len(partner_id_email):
                raise Warning(_('Email already exist'))
        if (empty == 0):
            raise Warning(_('You have to enter one of (Phone, Mobile, Mobile2, Email)'))
