# -*- coding: utf-8 -*-
{
    'name': "HR Custom Fields",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Acme",
    'website': "http://www.acme-group.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','hr'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/hr_custom_view.xml',
        'security/hr_custom_security.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}