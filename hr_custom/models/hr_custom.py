from openerp import models, fields, api, _
import re, sys
from openerp.osv import osv

class hr_employee(models.Model):
    # inherit = name (make changes in the same DB , do not create new DB)
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    #Add custom fields
    first_name                          = fields.Char(string='First Name', required=True)
    middle_name                         = fields.Char(string='Middle Name', required=True)
    last_name                           = fields.Char(string='Last Name', required=True)
    name_ar                             = fields.Char(string='Arabic Name',required=True)
    national_id                         = fields.Char(string='National ID', size=14, required=True)
    national_id_expiry_date             = fields.Date(string='National ID Expiry Date')
    passport_id                         = fields.Char(string='Passport #')
    address                             = fields.Char(string='Address')
    work_address                        = fields.Char(string='Work Address')
    work_location                       = fields.Char(string='Work Location')
    work_email                          = fields.Char(string='Work Email', size=240)
    home_phone                          = fields.Char(string='Home Phone')
    mobile_phone                        = fields.Char(string='Personal Mobile')
    work_mobile                         = fields.Char(string='Work Mobile')
    emergency_contact_name              = fields.Char(string='Emergency Contact Name')
    emergency_contact_phone             = fields.Char(string='Emergency Contact Phone')
    emergency_contact_relationship      = fields.Char(string='Emergency Contact Relationship')
    military_status                     = fields.Selection(string='Military Status',selection=[('exempted','Exempted'),('postponed','Postponed'),('completed','Completed')])
    # marital_status = fields.Char(string='Marital Status',selection=[('married','Married'),('divorced','Divorced'),('widowed','Widowed')])
    driver_license_number               = fields.Char(string='Driver License Id')
    driver_license_issue_date           = fields.Date(string='Driver License Issue Date')
    driver_license_expiry_date          = fields.Date(string='Driver License Expiry Date')
    driver_license_type                 = fields.Selection(string='Driver License Type',selection=[('regular','Regular'),('professional','Professional'),('1st degree','1st Degree'),('2nd degree','2nd Degree')])
    employee_id                         = fields.Char(string='Employee ID', help="Employee_ID will be generated upon save")
    social_insurance_number             = fields.Char(string='Social Insurance #',)
    country_of_assignment               = fields.Many2one(comodel_name='res.country', string='Country of Assignment', )
    job_code                            = fields.Char(string='Job Code')
    grade                               = fields.Char(string='Grade',)
    hire_date                           = fields.Date(string='Hire Date')
    termination_date                    = fields.Date(string='Termination Date')
    termination_reason                  = fields.Char(string='Termination Reason')
    exit_interview                      = fields.Boolean(string='Exit Interview')
    termination_documents               = fields.Boolean(string='Termination Documents Completed')
    eligible_for_rehire                 = fields.Boolean(string='Eligible for Rehire')
    eligible_for_rehire_reason          = fields.Char(string='Eligible for Rehire Reason')
    contract_type                       = fields.Selection(string='Contract Type',selection=[('definite_full_time','Definite Full Time'),('indefinite_full_time','Indefinite Full Time'),('part_time','Part Time'),('consultant','Consultant')])
    contract_sign_date                  = fields.Date(string='Contract Sign Date')
    contract_start_date                  = fields.Date(string='Contract Start Date')
    contract_expire_date                = fields.Date(string='Contract Expiry Date')
    probation_period_expire_date        = fields.Date(string='Probation Period Expiry Date')


    #functions

    @api.one
    @api.constrains('first_name', 'middle_name', 'last_name')
    def _check_name_duplication(self):
        employee_ids = self.search([('first_name', '=', str(self.first_name).strip().title()), ('middle_name', '=', str(self.middle_name).strip().title()), ('last_name', '=', str(self.last_name).strip().title()), ('id', '!=', self.id)])
        if len(employee_ids):
            raise Warning(_('Name already exist'))

    @api.one
    @api.constrains('employee_id')
    def _check_employee_id_duplication(self):
        employee_ids = self.search([('employee_id', '=', str(self.employee_id).strip()), ('id', '!=', self.id)])
        if len(employee_ids):
            raise Warning(_('Employee_ID already exist'))

    @api.one
    @api.constrains('work_email')
    def _check_work_email(self):
        if (self.work_email):
            # Check if exist
            employee_ids = self.search([('work_email', '=', str(self.work_email)), ('id', '!=', self.id)])
            if len(employee_ids):
                raise Warning(_('Work email already exist'))
            # Check email format
            if re.match("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", self.work_email) == None:
                raise Warning(_('Wrong email format'))


    @api.one
    @api.constrains('national_id')
    def _check_national_id(self):
        #Check length
        if len(str(self.national_id)) != 14:
            raise Warning(_('National ID should be 14 characters'))

        #check duplicate
        employee_ids = self.search([('national_id', '=', str(self.national_id)), ('id', '!=', self.id)])
        if len(employee_ids):
            raise Warning(_('National ID already exist'))

    def _generate_employee_id(self, cr):
        query = """ SELECT max(CAST(coalesce(employee_id, '0') AS integer)) as emp_id from hr_employee; """
        cr.execute(query)
        result = cr.dictfetchone()
        new_id = result['emp_id']+1

        if new_id == 1000:
            new_id = 1101
        else:
            if len(str(new_id)) == 2:
                new_id = '00'+str(new_id)

            if len(str(new_id)) == 3:
                new_id = '0'+str(new_id)


        return str(new_id)

    def create(self, cr , uid, data, context=None):
        if data.get("first_name"):
            data.update({'first_name': data.get("first_name").strip().title()})

        if data.get("middle_name"):
            data.update({'middle_name': data.get("middle_name").strip().title()})

        if data.get("last_name"):
            data.update({'last_name': data.get("last_name").strip().title()})
        # update hr.eployee.name
        fullName = str(data.get("first_name")).strip() + ' ' + str(data.get("middle_name")).strip() + ' ' + str(data.get("last_name")).strip()
        data.update({'name': fullName})
        try:
            if data.get("employee_id") == False:
                new_employee_id = self._generate_employee_id(cr)
                data.update({'employee_id': new_employee_id})
            else: #check if ID exist
                result = self.search(cr, uid,[('employee_id', '=', str(data.get("employee_id")).strip())], context=context)
                if len(result):
                    raise Exception("duplicate_id")

        except Exception as e:
            if e.message == 'duplicate_id':
                raise osv.except_osv(_("Warning"), _('An employee with the same ID already exist'))
            else:
                raise osv.except_osv(_("Warning"), _('An employee_id with the wrong format exist. Kindly change the ID in order to be able to generate new IDs \n' + '"Original Error: " '+e.message))

        return super(hr_employee, self).create(cr, uid, data, context=context)


    def write(self, cr , uid, ids, data, context=None):
        # get employee by ID
        employee = self.browse(cr, uid, ids, context=context)

        if data.get("first_name"):
            data.update({'first_name': data.get("first_name").strip().title()})
        else:
            data.update({'first_name': employee.first_name})


        if data.get("middle_name"):
            data.update({'middle_name': data.get("middle_name").strip().title()})
        else:
            data.update({'middle_name': employee.middle_name})

        if data.get("last_name"):
            data.update({'last_name': data.get("last_name").strip().title()})
        else:
            data.update({'last_name': employee.last_name})

        # update hr.eployee.name
        fullName = str(data.get("first_name")) + ' ' + str(data.get("middle_name")) + ' ' + str(data.get("last_name"))
        data.update({'name': fullName})

        try:

            if data.get("employee_id") == False:
                new_employee_id = self._generate_employee_id(cr)
                data.update({'employee_id': new_employee_id})
            else:
                result = self.search(cr, uid,[('employee_id', '=', str(data.get("employee_id")).strip()), ('id', '!=', ids[0])], context=context)
                if len(result):
                    raise Exception("duplicate_id")
        except Exception as e:
            if e.message == 'duplicate_id':
                raise osv.except_osv(_("Warning"), _('An employee with the same ID already exist'))
            else:
                raise osv.except_osv(_("Warning"), _('An employee_id with the wrong format exist. Kindly change the ID in order to be able to generate new IDs \n' + '"Original Error: " '+e.message))

        return super(hr_employee, self).write(cr, uid, ids, data, context=context)