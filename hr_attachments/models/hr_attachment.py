from openerp import models, fields


class hr_attachment_names(models.Model):
    _name = 'hr.attachment_names'
    _rec_name = 'attachment_name'

    attachment_name = fields.Char(string='Name', required=True)
    attachment_ids  = fields.One2many(comodel_name='hr.attachment', inverse_name='attachment_name_id')

class hr_attachment(models.Model):
    _name = 'hr.attachment'

    attachment_name_id = fields.Many2one(comodel_name= 'hr.attachment_names', string='Attachment name')
    attachment_file = fields.Binary(string='Attachment')
    employee_id  = fields.Many2one(comodel_name= 'hr.employee', string='Employee')

class hr_employee(models.Model):
    # Inherit from hr.employee
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    attachment_ids  = fields.One2many(comodel_name='hr.attachment', inverse_name='employee_id', string='Attachment')