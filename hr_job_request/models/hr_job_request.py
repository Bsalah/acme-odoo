from openerp import models, fields, api

class hr_job_request(models.Model):
    _name = 'hr.job_request'
    _rec_name = 'job_type'

    job_type                = fields.Selection(string="Job Type" ,selection=[('permanent','Permanent'),('temporary','Temporary')])#, required=True)
    requested_join_date     = fields.Date(string="Requested Join Date")
    budget                  = fields.Selection(string="Budget", selection=[('planned', 'Planned'), ('not_planned', 'Not Planned'), ('replacement', 'Replacement')])#, required=True)
    candidates              = fields.Integer(string='Number of Candidates', default=1)
    current_headcount       = fields.Char(string="Current Headcount")
    status                  = fields.Char(string="Status", default='Pending')
    reason_for_hiring       = fields.Char(string="Reason for Hiring")

    employee_id             = fields.Many2one(comodel_name='hr.employee', string='Replacer Name', )

    replacer_title          = fields.Char(string="Replacer Title")
    salary_suggested        = fields.Char(string="Salary Suggested")
    other_allowances        = fields.Char(string="Other Allowances")
    job_qualifications      = fields.Text(string="Job Qualifications")
    job_description         = fields.Text(string="Job Description")

    job_position_id         = fields.Many2one(comodel_name='hr.job', string='Job Position')

    @api.onchange('employee_id')
    def _onchange_employee(self):
        if self.employee_id:
            self.replacer_title = self.employee_id.job_id.name
