# -*- coding: utf-8 -*-
{
    'name': "School Training",

    'summary': """
        The training module""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Your Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr_attendance'],

    # always loaded
    'data': [
        'school_workflow.xml',
        'security/school_security.xml',
        'security/ir.model.access.csv',
        'views/school_view.xml',
        'views/teacher_view.xml',
        'views/student_view.xml',
        'views/course_view.xml',
        'views/employee_view.xml',
        'views/menuitems.xml',
        'templates.xml',
        'school_report.xml',
        'report/course_report.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}