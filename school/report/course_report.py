import datetime
import time
from openerp.osv import osv
from openerp.report import report_sxw

class course_report_print(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(course_report_print, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'get_length': self._get_length,
            'get_school_name': self._get_school_name,
            'get_course_se': self._get_course_se,
        })

    def _get_length(self, strings):
        return len(str(strings))

    def _get_school_name(self, school_name):
        school_pool = self.pool.get('school.school')
        # school_name = school_pool.browse(self.cr, self.uid, school_id).school_name
        school_id = school_pool.search(self.cr, self.uid, [('school_name', '=', school_name)])
        school_pool.write(self.cr, self.uid, school_id, {'no_st': 999999})
        return  school_name

    def _get_course_se(self):
        course_pool = self.pool.get('school.course')
        self.cr.execute("select name, code from school_course")
        # print(self.cr)
        print(self.cr.dictfetchall())
        # print(self.cr.fetchone())

        return "HAMADA"

class report_course_report(osv.AbstractModel):
    _name = 'report.school.report_course_temp'
    _inherit = 'report.abstract_report'
    _template = 'school.report_course_temp'
    _wrapped_report_class = course_report_print