from openerp import models, fields, api


class school_student(models.Model):
    _name = 'school.student'

    name = fields.Char(string='First Name', )
    image = fields.Binary(string='Image', )
    m_name = fields.Char(string='Middle Name', )
    l_name = fields.Char(string='Last Name', )
    gender = fields.Selection([('m', 'Male'), ('f', 'Female')], string='Gender', )
    birth_date = fields.Date(string='Birth Date', )
    age_1 = fields.Integer(string='Age', compute='_compute_age', store=True, )
    school_id = fields.Many2one(comodel_name='school.school', string='School', )

    @api.one
    @api.depends('birth_date')
    def _compute_age(self):
        self.age_1 = len(str(self.birth_date)) or 0