from openerp import models, fields, api, exceptions


class hr_employee(models.Model):
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    middle_name = fields.Char(string='Middle Name', required=True, )
    last_name = fields.Char(string='Last Name', required=True, )
    course_id = fields.Many2one(comodel_name='school.course', string='Course', )
    is_manager = fields.Boolean(string='Is Manager?')

    @api.one
    @api.constrains('name','middle_name', 'last_name')
    def check_name(self):
        emp_ids = self.search([('name', '=', str(self.name)), ('middle_name', '=', str(self.middle_name)), ('last_name', '=', str(self.last_name)), ('id', '!=', self.id)])
        print(emp_ids)
        if emp_ids:
            raise Warning('Name is already registered!')

    def create(self, cr , uid, vals, context=None):
        if vals.get('work_email'):
            # raise Warning Exception('FOO','Lorem ipsum dolor sit amet')
            print('1')
        else:
            vals.update({'work_email': 'x@y.z'})
        return super(hr_employee, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        vals.update({'work_phone': vals.get('name')})
        return super(hr_employee, self).write(cr, uid, ids, vals, context=context)

    @api.multi
    @api.depends('name', 'middle_name', 'last_name')
    def name_get(self):
        res = []
        for emp in self:
            res.append((emp.id, str(emp.name) + ' ' + str(emp.middle_name) + ' ' + str(emp.last_name)))
        return res


class hr_attendance(models.Model):
    _name = 'acme.attendance'
    _inherit = 'hr.attendance'

    acme_name = fields.Char(string='Acme Name', )