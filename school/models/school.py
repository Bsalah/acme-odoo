from openerp import models, fields, api, _


class shool(models.Model):
    _name = 'school.school'
    _rec_name = 'school_name'

    school_name = fields.Char(string='Name', )
    no_st = fields.Integer(string=_('Number of Students'), )

    @api.onchange('school_name')
    def onchange_school_name(self):
        if self.school_name:
            self.no_st = len(str(self.school_name))
        else:
            self.no_st = 0

