from openerp import models, fields, api


class school_course(models.Model):
    _name = 'school.course'

    name = fields.Char(string='Name', )
    code = fields.Char(string='Code', size=4, )
    desc = fields.Html(string='Description', )
    teacher_ids = fields.Many2many(comodel_name='school.teacher', relation='course_teacher', column1='course_id', column2='teacher_id', string='Teachers', )
    user_id = fields.Many2one(comodel_name='res.users', string='User', )
    state = fields.Selection(selection=[('draft', 'New'), ('open', 'In Progress'), ('done', 'Done')], string='State', default='draft', )

    # @api.one
    def set_open(self):
        self.state = 'open'

    # @api.one
    def set_done(self):
        self.state = 'done'

    # @api.one
    def set_draft(self):
        self.state = 'draft'

