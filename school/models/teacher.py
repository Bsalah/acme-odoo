from openerp import models, fields


class school_teacher(models.Model):
    _name = 'school.teacher'

    name = fields.Char(string='First Name', )
    email = fields.Char(string='Email', )
    is_active = fields.Boolean(string='Active ?', )
    role = fields.Selection(selection=[('j', 'Junior'), ('s', 'Senior')], string='Role', default='j', )
    # TODO: Add domain to avoid the same teacher
    supervisor_id = fields.Many2one(comodel_name='school.teacher', string='Supervisor', )
    course_ids = fields.Many2many(comodel_name='school.course', relation='course_teacher', column1='teacher_id', column2='course_id', string='Courses', )