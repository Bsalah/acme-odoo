from openerp import models, fields, api, _ , tools
from datetime import date, datetime

class hr_appraisal_objectives(models.Model):
    _name = 'hr.appraisal_objectives'

    objective               = fields.Char(string="Objectives")
    date_set                = fields.Date(string='Date Set')
    date_completion         = fields.Date(string="Date Completion")
    manager_assessment      = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    manager_comments        = fields.Char(string='Manager Comments')
    employee_comments       = fields.Char(string='Employee Comments')
    appraisal_id            = fields.Many2one(comodel_name= 'hr.appraisal', string='Employee')

class hr_appraisal(models.Model):
    _name = 'hr.appraisal'
    _rec_name = 'employee_id'

    objective_ids                   = fields.One2many(comodel_name='hr.appraisal_objectives', inverse_name='appraisal_id', string='Objectives')
    employee_id                     = fields.Many2one(comodel_name='hr.employee', string='Employee',required=True)
    state                           = fields.Selection(selection=[('draft', 'Draft'), ('waiting_approval', 'Waiting Approval'),('approved','Approved'),('closed','Sealed'), ('acknowledged','Acknowledge')], readonly='True', required='True', default='draft')
    job_position_id                 = fields.Many2one(comodel_name='hr.job', string='Job title')
    department_id                   = fields.Many2one(comodel_name='hr.department', string='Department')
    employment_date                 = fields.Date(string='Employment date')
    manager_id                      = fields.Many2one(comodel_name='hr.employee', string='Manager')
    manager_title                   = fields.Many2one(comodel_name='hr.job', string='Manager title')
    last_appraisal_date             = fields.Date(string='Last Appraisal Date')
    this_appraisal_date             = fields.Date(string='This Appraisal Date', required=True)
    date_started_current_role       = fields.Date(string='Date Started Current Role')
    purpose_of_review               = fields.Selection(string='Purpose of Review',selection=[('Annual','Annual'),('Other','Other (transfer,promotion)')])
    approval_date                   = fields.Date("Approval Date")
    close_date                      = fields.Date("Close Date")
    acknowledge_date                = fields.Date("Acknowledge Date")
    counter                         = fields.Integer(string="Counter", default=1)

    ###objective #1
    objective_1                     = fields.Html(string='Objective 1')
    objective_1_date_set            = fields.Date(string='Date Set')
    objective_1_date_completion     = fields.Date(string='Date of Completion')
    objective_1_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_1_manager_comments    = fields.Html(string='Manager Comments')
    objective_1_employee_comments   = fields.Html(string='Employee Comments')
    objective_1_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')

    ###objective #2
    objective_2                     = fields.Html(string='Objective 2')
    objective_2_date_set            = fields.Date(string='Date Set')
    objective_2_date_completion     = fields.Date(string='Date of Completion')
    objective_2_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_2_manager_comments    = fields.Html(string='Manager Comments')
    objective_2_employee_comments   = fields.Html(string='Employee Comments')
    objective_2_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')


    ###objective #3
    objective_3                     = fields.Html(string='Objective 3')
    objective_3_date_set            = fields.Date(string='Date Set')
    objective_3_date_completion     = fields.Date(string='Date of Completion')
    objective_3_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_3_manager_comments    = fields.Html(string='Manager Comments')
    objective_3_employee_comments   = fields.Html(string='Employee Comments')
    objective_3_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')


    ###objective #4
    objective_4                     = fields.Html(string='Objective 4')
    objective_4_date_set            = fields.Date(string='Date Set')
    objective_4_date_completion     = fields.Date(string='Date of Completion')
    objective_4_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_4_manager_comments    = fields.Html(string='Manager Comments')
    objective_4_employee_comments   = fields.Html(string='Employee Comments')
    objective_4_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')


    ###objective #5
    objective_5                     = fields.Html(string='Objective 5')
    objective_5_date_set            = fields.Date(string='Date Set')
    objective_5_date_completion     = fields.Date(string='Date of Completion')
    objective_5_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_5_manager_comments    = fields.Html(string='Manager Comments')
    objective_5_employee_comments   = fields.Html(string='Employee Comments')
    objective_5_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')


    ###objective #6
    objective_6                     = fields.Html(string='Objective 6')
    objective_6_date_set            = fields.Date(string='Date Set')
    objective_6_date_completion     = fields.Date(string='Date of Completion')
    objective_6_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_6_manager_comments    = fields.Html(string='Manager Comments')
    objective_6_employee_comments   = fields.Html(string='Employee Comments')
    objective_6_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')


    ###objective #7
    objective_7                     = fields.Html(string='Objective 7')
    objective_7_date_set            = fields.Date(string='Date Set')
    objective_7_date_completion     = fields.Date(string='Date of Completion')
    objective_7_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_7_manager_comments    = fields.Html(string='Manager Comments')
    objective_7_employee_comments   = fields.Html(string='Employee Comments')
    objective_7_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')

    ###objective #8
    objective_8                     = fields.Html(string='Objective 8')
    objective_8_date_set            = fields.Date(string='Date Set')
    objective_8_date_completion     = fields.Date(string='Date of Completion')
    objective_8_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_8_manager_comments    = fields.Html(string='Manager Comments')
    objective_8_employee_comments   = fields.Html(string='Employee Comments')
    objective_8_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')


    ###objective #9
    objective_9                     = fields.Html(string='Objective 9')
    objective_9_date_set            = fields.Date(string='Date Set')
    objective_9_date_completion     = fields.Date(string='Date of Completion')
    objective_9_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_9_manager_comments    = fields.Html(string='Manager Comments')
    objective_9_employee_comments   = fields.Html(string='Employee Comments')
    objective_9_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')


    ###objective #10
    objective_10                     = fields.Html(string='Objective 10')
    objective_10_date_set            = fields.Date(string='Date Set')
    objective_10_date_completion     = fields.Date(string='Date of Completion')
    objective_10_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_10_manager_comments    = fields.Html(string='Manager Comments')
    objective_10_employee_comments   = fields.Html(string='Employee Comments')
    objective_10_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')


    ###objective #11
    objective_11                     = fields.Html(string='Objective 11')
    objective_11_date_set            = fields.Date(string='Date Set')
    objective_11_date_completion     = fields.Date(string='Date of Completion')
    objective_11_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_11_manager_comments    = fields.Html(string='Manager Comments')
    objective_11_employee_comments   = fields.Html(string='Employee Comments')
    objective_11_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')


    ###objective #12
    objective_12                     = fields.Html(string='Objective 12')
    objective_12_date_set            = fields.Date(string='Date Set')
    objective_12_date_completion     = fields.Date(string='Date of Completion')
    objective_12_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_12_manager_comments    = fields.Html(string='Manager Comments')
    objective_12_employee_comments   = fields.Html(string='Employee Comments')
    objective_12_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')


    ###objective #13
    objective_13                     = fields.Html(string='Objective 13')
    objective_13_date_set            = fields.Date(string='Date Set')
    objective_13_date_completion     = fields.Date(string='Date of Completion')
    objective_13_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_13_manager_comments    = fields.Html(string='Manager Comments')
    objective_13_employee_comments   = fields.Html(string='Employee Comments')
    objective_13_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')


    ###objective #14
    objective_14                     = fields.Html(string='Objective 14')
    objective_14_date_set            = fields.Date(string='Date Set')
    objective_14_date_completion     = fields.Date(string='Date of Completion')
    objective_14_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_14_manager_comments    = fields.Html(string='Manager Comments')
    objective_14_employee_comments   = fields.Html(string='Employee Comments')
    objective_14_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')


    ###objective #15
    objective_15                     = fields.Html(string='Objective 15')
    objective_15_date_set            = fields.Date(string='Date Set')
    objective_15_date_completion     = fields.Date(string='Date of Completion')
    objective_15_manager_assessment  = fields.Selection(string='Manager Assessment',selection=([('exceeded','Exceeded'),('partly_achieved','Partly Achieved'),('achieved','Achieved'),('not_achieved','not Achieved')]))
    objective_15_manager_comments    = fields.Html(string='Manager Comments')
    objective_15_employee_comments   = fields.Html(string='Employee Comments')
    objective_15_acknowledgment_comments   = fields.Html(string='Acknowledgment Comments')

    #Leadership
    foster_collaboration            = fields.Selection(string='a. Fosters Collaboration and Open-mindedness',selection=([('Outstanding','Outstanding'),('Exceeds','Exceeds'),('Satisfactory','Satisfactory'),('Some Improvement/Development Needed','Some Improvement/Development Needed'),('Unsatisfactory','Unsatisfactory'),('Unable to Rate or Not Applicable','Unable to Rate or Not Applicable')]))
    foster_collaboration_comments   = fields.Char(string='Comments')

    empowers                        = fields.Selection(string='b. Empowers and Inspires People',selection=([('Outstanding','Outstanding'),('Exceeds','Exceeds'),('Satisfactory','Satisfactory'),('Some Improvement/Development Needed','Some Improvement/Development Needed'),('Unsatisfactory','Unsatisfactory'),('Unable to Rate or Not Applicable','Unable to Rate or Not Applicable')]))
    empowers_comments               = fields.Char(string='Comments')

    delivers                        = fields.Selection(string='a. Delivers Growth',selection=([('Outstanding','Outstanding'),('Exceeds','Exceeds'),('Satisfactory','Satisfactory'),('Some Improvement/Development Needed','Some Improvement/Development Needed'),('Unsatisfactory','Unsatisfactory'),('Unable to Rate or Not Applicable','Unable to Rate or Not Applicable')]))
    delivers_comments               = fields.Char(string='Comments')

    drives_for_results              = fields.Selection(string='b. Drives for   Results',selection=([('Outstanding','Outstanding'),('Exceeds','Exceeds'),('Satisfactory','Satisfactory'),('Some Improvement/Development Needed','Some Improvement/Development Needed'),('Unsatisfactory','Unsatisfactory'),('Unable to Rate or Not Applicable','Unable to Rate or Not Applicable')]))
    drives_for_results_comments     = fields.Char(string='Comments')

    entrepreneurial                 = fields.Selection(string='a. Entrepreneurial',selection=([('Outstanding','Outstanding'),('Exceeds','Exceeds'),('Satisfactory','Satisfactory'),('Some Improvement/Development Needed','Some Improvement/Development Needed'),('Unsatisfactory','Unsatisfactory'),('Unable to Rate or Not Applicable','Unable to Rate or Not Applicable')]))
    entrepreneurial_comments        = fields.Char(string='Comments')

    continues_improvement           = fields.Selection(string='b. Enables Continuous Improvement',selection=([('Outstanding','Outstanding'),('Exceeds','Exceeds'),('Satisfactory','Satisfactory'),('Some Improvement/Development Needed','Some Improvement/Development Needed'),('Unsatisfactory','Unsatisfactory'),('Unable to Rate or Not Applicable','Unable to Rate or Not Applicable')]))
    continues_improvement_comments  = fields.Char(string='Comments')

    understand_customers            = fields.Selection(string='a. Understands Customers Perspectives and Needs',selection=([('Outstanding','Outstanding'),('Exceeds','Exceeds'),('Satisfactory','Satisfactory'),('Some Improvement/Development Needed','Some Improvement/Development Needed'),('Unsatisfactory','Unsatisfactory'),('Unable to Rate or Not Applicable','Unable to Rate or Not Applicable')]))
    understand_customers_comments   = fields.Char(string='Comments')

    drives_service                  = fields.Selection(string='b. Drives Service Excellence',selection=([('Outstanding','Outstanding'),('Exceeds','Exceeds'),('Satisfactory','Satisfactory'),('Some Improvement/Development Needed','Some Improvement/Development Needed'),('Unsatisfactory','Unsatisfactory'),('Unable to Rate or Not Applicable','Unable to Rate or Not Applicable')]))
    drives_service_comments         = fields.Char(string='Comments')

    create_lead_strategic           = fields.Selection(string='a. Creates and Leads Strategic Direction',selection=([('Outstanding','Outstanding'),('Exceeds','Exceeds'),('Satisfactory','Satisfactory'),('Some Improvement/Development Needed','Some Improvement/Development Needed'),('Unsatisfactory','Unsatisfactory'),('Unable to Rate or Not Applicable','Unable to Rate or Not Applicable')]))
    create_lead_strategic_comments  = fields.Char(string='Comments')

    demonstrates                    = fields.Selection(string='b. Demonstrates Effective Business Management',selection=([('Outstanding','Outstanding'),('Exceeds','Exceeds'),('Satisfactory','Satisfactory'),('Some Improvement/Development Needed','Some Improvement/Development Needed'),('Unsatisfactory','Unsatisfactory'),('Unable to Rate or Not Applicable','Unable to Rate or Not Applicable')]))
    demonstrates_comments           = fields.Char(string='Comments')

    overall_rating                  = fields.Selection(string='Overall Rating',selection=([('Exceeds Expectations','Exceeds Expectations'),('Meets Expectations','Meets Expectations'),('Some Improvement Needed','Some Improvement Needed'),('Unsatisfactory','Unsatisfactory')]))
    development_plan                = fields.Text(string='Career/Personal Development Plan')
    required_training               = fields.Text(string='required training ')
    long_term_objective             = fields.Text(string='Long Term Objectives')
    manager_comment                 = fields.Text(string="Manager's Comments")


    # Checkup fields
    current_user_group          = fields.Char(string="Current User Group", compute="_compute_current_user_group")
    assessment_readonly         = fields.Char(string="Assessment Readonly", compute= '_compute_assessment_readonly_check')
    objective_readonly          = fields.Char(string="Objective Readonly", compute='_compute_objective_readonly')
    date_set_readonly           = fields.Char(string="Date Set Readonly", compute='_compute_date_set_readonly')
    date_completion_readonly    = fields.Char(string="Date of Completion Readonly", compute='_compute_date_completion_readonly')
    manager_assessment_readonly = fields.Char(string="Manager Assessment Readonly", compute='_compute_manager_assessment_readonly')
    manager_comments_readonly   = fields.Char(string="Manager Comments Readonly", compute='_compute_manager_comments_readonly')
    employee_comments_readonly  = fields.Char(string="Employee Comments Readonly", compute='_compute_employee_comments_readonly')
    acknowledgment_comments_readonly  = fields.Char(string="acknowledged Comments Readonly", compute='_compute_acknowledgment_comments_readonly')
    acknowledged_check          = fields.Char(string="Acknowledge Check", compute='_compute_acknowledged_check', default='True')



    def add_objective(self, cr, uid, ids, context=None):
        appraisal = self.browse(cr, uid, ids[0], context=None)
        if appraisal.counter < 16:
            print(appraisal.counter + 1)
            self.write(cr, uid, ids, {
                'counter': appraisal.counter + 1,
            }, context=context)
            return True

    def remove_objective(self, cr, uid, ids, context=None):
        appraisal = self.browse(cr, uid, ids[0], context=None)
        if appraisal.counter > 1:
            appraisal = self.browse(cr, uid, ids[0], context=None)
            self.write(cr, uid, ids, {
                'objective_'+str(appraisal.counter):                     None,
                'objective_'+str(appraisal.counter)+'_date_set': None,
                'objective_'+str(appraisal.counter)+'_date_completion': None,
                'objective_'+str(appraisal.counter)+'_manager_assessment': None,
                'objective_'+str(appraisal.counter)+'_manager_comments': None,
                'objective_'+str(appraisal.counter)+'_employee_comments': None,
                'counter': appraisal.counter - 1
            }, context=context)
            return True




    def submit_manager(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {
            'state': 'waiting_approval'
        }, context=context)
        appraisal = self.browse(cr, uid, ids[0], context=None)
        vals = {
            "mail_to_id":appraisal.employee_id.parent_id.user_id.id,
            "subject": "[Appraisal] Setting Objectives - Review and Approval",
            "body": """Dear """+str(appraisal.employee_id.parent_id.name)+""", \n\n\tKindly note that """+str(appraisal.employee_id.name)+""" has submitted his objectives for your review and approval. Your approval will ceil assigned objectives till the end of the year.\n\tIf you wish to review please click on below link. \n\nMOTIV8"""
        }
        self.send_email(cr,uid, vals)
        return True

    def set_draft(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {
            'state': 'draft'
        }, context=context)

        appraisal = self.browse(cr, uid, ids[0], context=None)
        vals = {
            "mail_to_id":  appraisal.employee_id.user_id.id,
            "subject":  "[Appraisal] Re-set Objectives",
            "body":     """Dear """+str(appraisal.employee_id.name)+""",\n\n\tPlease note that """+str(appraisal.employee_id.parent_id.name)+""" has returned your objectives for resetting. If you wish to log into system now please click on below linkp.\n\nMOTIV8"""
        }
        self.send_email(cr,uid, vals)
        return True

    def set_approve(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {
            'state':            'approved',
            'approval_date':    datetime.today()
        }, context=context)

        appraisal = self.browse(cr, uid, ids[0], context=None)
        vals = {
            "mail_to_id":  appraisal.employee_id.user_id.id,
            "subject":  "[Appraisal] Objectives Approved",
            "body":     """Dear """+str(appraisal.employee_id.name)+""",\n\n\tPlease note that """+str(appraisal.employee_id.parent_id.name)+""" has approved your objectives and they will be ceilied till the end of year for performance appraisal stage.\n\tIf you wish to reveiw log into system; please click on below link.\n\nMOTIV8"""
        }
        self.send_email(cr,uid, vals)

        return True

    def set_close(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {
            'state':            'closed',
            'close_date':    datetime.today()
        }, context=context)

        appraisal = self.browse(cr, uid, ids[0], context=None)
        vals = {
            "mail_to_id":  appraisal.employee_id.user_id.id,
            "subject":  "[Appraisal] Acknowledgement",
            "body":     """Dear """+str(appraisal.employee_id.name)+""",\n\n\tPlease note that """+str(appraisal.employee_id.parent_id.name)+""" closed your performance appraisal.\n\tFor acknowledgment  log into system; please click on below link.\n\nMOTIV8"""
        }
        self.send_email(cr,uid, vals)

        return True

    def set_acknowledged(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {
            'state':                'acknowledged',
            'acknowledge_date':     datetime.today()
        }, context=context)

        appraisal = self.browse(cr, uid, ids[0], context=None)
        vals = {
            "mail_to_id":  appraisal.employee_id.parent_id.user_id.id,
            "subject":  "[Appraisal] "+str(appraisal.employee_id.name)+"'s Acknowledgment",
            "body":     """Dear """+str(appraisal.employee_id.parent_id.name)+""",\n\n\tKindly note that """+str(appraisal.employee_id.name)+""" has acknowledged his appraisal plan.\n\nMOTIV8"""
        }
        self.send_email(cr,uid, vals)

        return True


    @api.one
    @api.constrains('employee_id', 'this_appraisal_date')
    def _check_appraisal_duplication(self):
        appraisals = self.search([('employee_id', '=', self.employee_id.id), ('id', '!=', self.id), ('this_appraisal_date', 'ilike', self.this_appraisal_date[:4])])
        if appraisals:
            raise Warning(_('Appraisal already exist for the same year'))

    @api.onchange('employee_id')
    def _onchange_employee(self):
        if self.employee_id:
            self.job_position_id = self.employee_id.job_id.id
            self.department_id = self.employee_id.department_id.id
            self.manager_id = self.employee_id.parent_id.id
            self.manager_title = self.employee_id.parent_id.job_id.id
            self.employment_date = self.employee_id.hire_date

    def create(self, cr , uid, vals, context=None):
        employee_obj = self.pool.get('hr.employee').browse(cr, uid, vals.get('employee_id'), context=context)
        vals.update({'job_position_id': employee_obj.job_id.id})
        vals.update({'department_id': employee_obj.department_id.id})
        vals.update({'manager_id': employee_obj.parent_id.id})
        vals.update({'manager_title': employee_obj.parent_id.job_id.id})
        vals.update({'employment_date': employee_obj.hire_date})
        id = super(hr_appraisal, self).create(cr, uid, vals, context=context)
        if id:
            vals = {
                "mail_to_id":  employee_obj.user_id.id,
                "subject":  "[Appraisal] Setting Objectives",
                "body":     """Dear """ +str(employee_obj.name)+""",\n\n\tIt is this time of year !! lets start setting year objectives for a fruitfull productive business year :).\n\tAs a reminder your objectives has to be SMART, defined and cascaded from company objectives. If you wish to log into system now please click on below link.\n\nMOTIV8"""
            }
            self.send_email(cr,uid,vals)

            return id

    # def write(self, cr, uid, ids, vals, context=None):
    #     print(vals)
        # if vals.get('objective_1_manager_comments') == self.objective_1_manager_comments:
        #     print("Same")
        # else:
        #     print("Changeee")

    def _field_check(self, state, current_user_group):
        if self.this_appraisal_date:
            checkup_date =  datetime.strptime(self.this_appraisal_date[:4]+'-12-1', "%Y-%m-%d")
            if (datetime.today() > checkup_date) and (self.state == state) and (self.current_user_group == current_user_group):
                return 'False'
            else:
                return 'True'
        else:
            return 'True'


    def _compute_assessment_readonly_check(self):
        self.assessment_readonly = self._field_check('approved','officer')

    def _compute_acknowledged_check(self):
        self.acknowledged_check = self._field_check('closed','employee')

    #function returns ('manager' or 'officer' or 'employee')
    def _get_current_user_group(self):
        #get all group_ids where group_category = Human Recourse
        query1 = """ SELECT rg.id from res_groups as rg, ir_module_category as cat
                    where
                    rg.category_id = cat.id and
                    cat.name = 'Human Resources'
                    """
        self._cr.execute(query1)
        HR_group_ids        = self._cr.fetchall()



        #get group_ids for user where group_category = Human Recourse
        query2 = """ select rgu.gid from res_groups_users_rel as rgu, res_groups as rg, ir_module_category as ir
                        where
                        rgu.gid = rg.id and
                        rgu.uid = """ + str(self.env.user.id) + """ and
                        rg.category_id = ir.id and
                        ir.name = 'Human Resources'
                    """
        self._cr.execute(query2)
        HR_user_group_ids        = self._cr.fetchall()


        # subtract 2 lists to find out current user group
            # if Difference = 2  ==> user = employee
            # if Difference = 1  ==> user = officer
            # if Difference = 0  ==> user = manager

        if len(tuple(set(HR_group_ids) - set(HR_user_group_ids))) == 2:
            return 'employee'
        elif len(tuple(set(HR_group_ids) - set(HR_user_group_ids))) == 1:
            return 'officer'
        else:
            return 'manager'

    @api.one
    @api.depends('employee_id') #used any field for the function to work
    def _compute_current_user_group(self):
        self.current_user_group = self._get_current_user_group()


##############################################################################################################################


    @api.one
    @api.depends('employee_id')
    def _compute_objective_readonly(self):
        # Objective
        if (self.state == 'approved' or self.state == 'closed' or self.state == 'acknowledged') or (self.state == 'waiting_approval' and self.current_user_group == 'employee'):
            self.objective_readonly = 'True'
        else:
            self.objective_readonly = 'False'

    @api.one
    @api.depends('state')
    def _compute_date_set_readonly(self):
        # Date Set
        if (self.state == 'approved' or self.state == 'closed' or self.state == 'acknowledged') or (self.state == 'waiting_approval' and self.current_user_group == 'employee'):
            self.date_set_readonly = 'True'
        else:
            self.date_set_readonly = 'False'

    @api.one
    @api.depends('state')
    def _compute_date_completion_readonly(self):
        # Date of Completion
        if (self.state == 'approved') and (self.current_user_group == 'officer'):
            self.date_completion_readonly = 'False'
        else:
            self.date_completion_readonly = 'True'


    @api.one
    @api.depends('state')
    def _compute_manager_assessment_readonly(self):
        # Manager Assessment
        if (self.state == 'approved') and (self.current_user_group == 'officer'):
            self.manager_assessment_readonly = 'False'
        else:
            self.manager_assessment_readonly = 'True'


    @api.one
    @api.depends('state')
    def _compute_manager_comments_readonly(self):
        # Manager Comments
        if (self.state == 'approved') and (self.current_user_group == 'officer'):
            self.manager_comments_readonly = 'False'
        else:
            self.manager_comments_readonly = 'True'

    @api.one
    @api.depends('state')
    def _compute_employee_comments_readonly(self):
        # Employee Comments
        if (self.state == 'approved') and (self.current_user_group == 'employee'):
            self.employee_comments_readonly = 'False'
        else:
            self.employee_comments_readonly = 'True'

    @api.one
    @api.depends('state')
    def _compute_acknowledgment_comments_readonly(self):
        # Employee acknowledged Comments
        if (self.state == 'closed') and (self.current_user_group == 'employee'):
            self.acknowledgment_comments_readonly = 'False'
        else:
            self.acknowledgment_comments_readonly = 'True'

    # Send E-Mail
    def send_email(self, cr, uid, vals, context=None):
        if vals["mail_to_id"]:
            user = self.pool.get('res.users').browse(cr, uid, vals["mail_to_id"], context=None)
            tools.email_send\
            (
                'noreply@openerp.com',
                [user.login, ],
                vals["subject"],
                vals["body"]
            )
            # (
            #     'noreply@openerp.com',
            #     [user.login, ],
            #     vals["subject"],
            #     vals["body"],
            #     None,
            #     None,
            #     False,
            #     None,
            #     None,
            #     None,
            #     False,
            #     False,
            #     'plain',
            #     'HTML', #headers,
            # )