from openerp import models, fields, api, _ , tools
from openerp.osv import osv

class res_partner(osv.osv):
    """ Inherits partner and adds CRM information in the partner form """
    _name = 'res.partner'
    _inherit = 'res.partner'

    property_account_receivable     = fields.Many2one(comodel_name= 'account.account', string='Account Receivable',required=False)
    property_account_payable     = fields.Many2one(comodel_name= 'account.account', string='Account Payable',required=False)


    @api.one
    @api.constrains('name')
    def _check_name_duplication(self):
        employee_ids = self.search([('name', '=', str(self.name)), ('id', '!=', self.id)])
        if len(employee_ids):
            raise Warning(_('Name already exist'))

class sale_order(osv.osv):
    _name = "sale.order"
    _inherit = "sale.order"

    def action_button_confirm(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self._check_customer(cr, uid, ids)
        self.signal_workflow(cr, uid, ids, 'order_confirm')
        return True

    # @api.one
    # @api.constrains('partner_id')
    def _check_customer(self, cr, uid, ids):
        # raise Warning(ids)

        object = self.pool.get('sale.order').browse(cr, uid, ids[0], context=None)
        # print(object)
        if( (isinstance(object.partner_id.phone, (bool)) and isinstance(object.partner_id.mobile, (bool))) or ( not object.partner_id.street) or ( not object.partner_id.email) or (not object.partner_id.property_account_receivable) or (not object.partner_id.property_account_payable) ):
            # raise Warning(_("Please make sure the following fields are not empty in the Customer data ( Mobile/Phone, Address and Email )"))
            raise osv.except_osv(
                        _('WARNING'),
                        _('Please make sure the following fields are not empty in the Customer data /n ( Mobile/Phone, Address and Email )'))
        return True


