from openerp.osv import osv
from openerp.report import report_sxw
from lxml import html
import datetime

class bank_report(report_sxw.rml_parse):
    _name="bank.report"
    def __init__(self, cr, uid, name, context):
        super(bank_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            # 'time': time,
            # 'get_length': self._get_length,
            'get_payslips': self.get_payslips,
            'get_payslip_amount':self.get_payslip_amount
            # 'get_counter_list': self._get_counter_list
        })

    def get_payslips(self,batch_id, query_str):
        result = []
        # payslips_ids = self.pool.get('hr.payslip').search(self.cr, self.uid, [('payslip_run_id', '=', batch_id)])
        query2 = """ select ps.id from hr_payslip as ps, hr_employee as emp
                        where
                        ps.payslip_run_id = """ + str(batch_id) + """ and
                        ps.employee_id = emp.id and
                        emp.bank_account_id """ + query_str
        self.cr.execute(query2)
        payslips_ids  = self.cr.fetchall()
        for payslip_id in payslips_ids:
            result.append(self.pool.get('hr.payslip').browse(self.cr, self.uid, payslip_id, context=None))
        return result

    def get_payslip_amount(self,slip_id):
        # result = self.pool.get('hr.payslip.line').search(self.cr, self.uid, [('slip_id', '=', slip_id),("code",'=',"NET")], context=None)
        # return result
        query2 = """ select amount from hr_payslip_line
                        where
                        slip_id = """ + str(slip_id) + """ and
                        code = 'NET'
                    """
        self.cr.execute(query2)
        result  = self.cr.fetchone()
        if result:
            return result[0]
        else:
            return 0


class report_bank_report(osv.AbstractModel):
    _name       = 'report.hr_bank_reports.report_bank_temp'
    _inherit    = 'report.abstract_report'
    _template   = 'hr_bank_reports.report_bank_temp'
    _wrapped_report_class = bank_report

###########################################################################
class cash_report(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(cash_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            # 'time': time,
            # 'get_length': self._get_length,
            'get_payslips': self.get_payslips,
            'get_payslip_amount':self.get_payslip_amount
            # 'get_counter_list': self._get_counter_list
        })

    def get_payslips(self,batch_id, query_str):
        result = []
        query2 = """ select ps.id from hr_payslip as ps, hr_employee as emp
                        where
                        ps.payslip_run_id = """ + str(batch_id) + """ and
                        ps.employee_id = emp.id and
                        emp.bank_account_id """ + query_str
        self.cr.execute(query2)
        payslips_ids  = self.cr.fetchall()
        for payslip_id in payslips_ids:
            result.append(self.pool.get('hr.payslip').browse(self.cr, self.uid, payslip_id, context=None))
        return result


    def get_payslip_amount(self,slip_id):
        # result = self.pool.get('hr.payslip.line').search(self.cr, self.uid, [('slip_id', '=', slip_id),("code",'=',"NET")], context=None)
        # return result
        query2 = """ select amount from hr_payslip_line
                        where
                        slip_id = """ + str(slip_id) + """ and
                        code = 'NET'
                    """
        self.cr.execute(query2)
        result  = self.cr.fetchone()
        if result:
            return result[0]
        else:
            return 0

class report_cash_report(osv.AbstractModel):
    _name       = 'report.hr_bank_reports.report_cash_temp'
    _inherit    = 'report.abstract_report'
    _template   = 'hr_bank_reports.report_cash_temp'
    _wrapped_report_class = cash_report

###########################################################################
class combined_report(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(combined_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            # 'time': time,
            # 'get_length': self._get_length,
            'get_payslips': self.get_payslips,
            'get_payslip_amount':self.get_payslip_amount
            # 'get_counter_list': self._get_counter_list
        })

    def get_payslips(self,batch_id, query_str):
        result = []
        query2 = """ select ps.id from hr_payslip as ps, hr_employee as emp
                        where
                        ps.payslip_run_id = """ + str(batch_id) + """ and
                        ps.employee_id = emp.id """
        self.cr.execute(query2)
        payslips_ids  = self.cr.fetchall()
        for payslip_id in payslips_ids:
            result.append(self.pool.get('hr.payslip').browse(self.cr, self.uid, payslip_id, context=None))
        return result


    def get_payslip_amount(self,slip_id):
        # result = self.pool.get('hr.payslip.line').search(self.cr, self.uid, [('slip_id', '=', slip_id),("code",'=',"NET")], context=None)
        # return result
        query2 = """ select amount from hr_payslip_line
                        where
                        slip_id = """ + str(slip_id) + """ and
                        code = 'NET'
                    """
        self.cr.execute(query2)
        result  = self.cr.fetchone()
        if result:
            return result[0]
        else:
            return 0

class report_combined_report(osv.AbstractModel):
    _name       = 'report.hr_bank_reports.report_combined_temp'
    _inherit    = 'report.abstract_report'
    _template   = 'hr_bank_reports.report_combined_temp'
    _wrapped_report_class = combined_report

###########################################################################
class detailed_report(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(detailed_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            # 'time': time,
            # 'get_length': self._get_length,
            'get_payslips': self.get_payslips,
            'get_payslip_amount':self.get_payslip_amount,
            'get_payslip_rules': self.get_payslip_rules
        })

    def get_payslips(self,batch_id, query_str):
        result = []
        query2 = """ select ps.id from hr_payslip as ps, hr_employee as emp
                        where
                        ps.payslip_run_id = """ + str(batch_id) + """ and
                        ps.employee_id = emp.id """
        self.cr.execute(query2)
        payslips_ids  = self.cr.fetchall()
        for payslip_id in payslips_ids:
            result.append(self.pool.get('hr.payslip').browse(self.cr, self.uid, payslip_id, context=None))
        return result


    def get_payslip_amount(self,slip_id):
        # result = self.pool.get('hr.payslip.line').search(self.cr, self.uid, [('slip_id', '=', slip_id),("code",'=',"NET")], context=None)
        # return result
        query2 = """ select name,amount from hr_payslip_line
                        where
                        slip_id = """ + str(slip_id)
        self.cr.execute(query2)
        result  = self.cr.fetchone()
        # print(query2)
        if result:
            return result[0]
        else:
            return 0

    def get_payslip_rules(self,slip_id):
        # result = self.pool.get('hr.payslip.line').search(self.cr, self.uid, [('slip_id', '=', slip_id),("code",'=',"NET")], context=None)
        # return result
        query2 = """ select name,amount from hr_payslip_line
                        where
                        slip_id = """ + str(slip_id)
        self.cr.execute(query2)
        result  = self.cr.fetchone()
        print(result)
        if result:
            return result[0]
        else:
            return 0

class report_detailed_report(osv.AbstractModel):
    _name       = 'report.hr_bank_reports.report_detailed_temp'
    _inherit    = 'report.abstract_report'
    _template   = 'hr_bank_reports.report_detailed_temp'
    _wrapped_report_class = detailed_report


#######################################################################################################
from dateutil.relativedelta import relativedelta
class visa_report(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(visa_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            # 'time': time,
            # 'get_length': self._get_length,
            'get_expiry_visa': self.get_expiry_visa
            # 'get_payslip_amount':self.get_payslip_amount
            # 'get_payslip_rules': self.get_payslip_rules
        })

    def get_expiry_visa(self):
        result = []
        date_after_3month = datetime.date.today()+ relativedelta(months=3)
        query2 = """ select id from hr_contract
         where visa_expire < ' """ +  str(date_after_3month) +  """ '
         """
        # print(query2)
        self.cr.execute(query2)


        # print(date_after_month)
        contracts_ids  = self.cr.fetchall()
        for contract_id in contracts_ids:
            result.append(self.pool.get('hr.contract').browse(self.cr, self.uid, contract_id, context=None))
        return result


class report_visa_report(osv.AbstractModel):
    _name       = 'report.hr_bank_reports.report_visa_temp'
    _inherit    = 'report.abstract_report'
    _template   = 'hr_bank_reports.report_visa_temp'
    _wrapped_report_class = visa_report